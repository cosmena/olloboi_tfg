Web App Audiovisual Escolar
===========================

Aplicación web de soporte do audiovisual escolar galego:
- Centos de vídeos inscritos
- Festivais e concursos
- Centros educativos de toda Galicia
- Inscribe os teus propios vídeos
- Marca os teus favoritos e os xa visionados
- Organiza o teu propio concurso audiovisual
- e moito máis

Xorde como ferramenta para facilitar a organización e difusión do festival Olloboi. 
Amplíase durante o desenvolvemento como Traballo de Fin de Grao de Enxeñaría Informática pola UDC,
obtendo unha calificación de Matrícula de Honra.
Plantéxase unha redefinición do proxecto e a procura de colaboración que permitan a súa coninuidade.

Es desenvolvedor, deseñador, realizador, espectador e che gustaría colaborar?
Contacta co desenvolvedor orixinal: Óscar Núñez Aguado (cosmena@gmail.com)

A Symfony project created on February 9, 2017, 8:38 pm.

Tecnoloxías: DAD, MVC, Symfony, PHP, JavaScript, jQuery, ORM, Doctrine, Twig, MySQL, YouTube, Vimeo, Google Maps