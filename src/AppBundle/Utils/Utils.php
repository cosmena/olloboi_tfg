<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Utils/Utils.php
namespace AppBundle\Utils;

class Utils
{

  public function multiplica($video)
  {
    $video->setTitle("Qué titulazo!!");
    return 5 * 5;
  }
  /**
    * @Assert\IsTrue(message = "A Url introducida non parece de Vimeo ou Youtube.", groups = {"UrlVimeoYoutube"})
    */
  public function isYoutubeOrVimeo()
  {
    // Examina a url e comproba se é de Youtube ou de Vimeo.
    //      En Youtube pode fallar nas listas
    //      Garda o Proveedor e acorta a URL ou devolve NULL
    //      !!! PROBAR MÁIS CONCIENZUDAMENTE. REVISAR E MELLORAR
    // é YOUTUBE?
    if ($recorte = strpos($this->url, "list")) {
      $this->url = substr($this->url, 0, $recorte);
    }
    $regexstr = '~
    # Match Youtube link and embed code
    (?:				 # Group to match embed codes
       (?:<iframe [^>]*src=")?	 # If iframe match up to first quote of src
       |(?:				 # Group to match if older embed
          (?:<object .*>)?		 # Match opening Object tag
          (?:<param .*</param>)*     # Match all param tags
          (?:<embed [^>]*src=")?     # Match embed tag to the first quote of src
       )?				 # End older embed code group
    )?				 # End embed code groups
    (?:				 # Group youtube url
       https?:\/\/		         # Either http or https
       (?:[\w]+\.)*		         # Optional subdomains
       (?:               	         # Group host alternatives.
           youtu\.be/      	         # Either youtu.be,
           | youtube\.com		 # or youtube.com
           | youtube-nocookie\.com	 # or youtube-nocookie.com
       )				 # End Host Group
       (?:\S*[^\w\-\s])?       	 # Extra stuff up to VIDEO_ID
       ([\w\-]{11})		         # $1: VIDEO_ID is numeric
       [^\s]*			 # Not a space
    )				 # End group
    "?				 # Match end quote if part of src
    (?:[^>]*>)?			 # Match any extra stuff up to close brace
    (?:				 # Group to match last embed code
       </iframe>		         # Match the end of the iframe
       |</embed></object>	         # or Match the end of the older embed
    )?				 # End Group of last bit of embed code
    ~ix';

    $iframestr = '$1';

    $Resultado=preg_replace($regexstr, $iframestr, $this->url);
    // preg_replace: si se cumplen las reglas devuelve el código del vídeo
    //                sino devuelve la url de entrada
    if ($Resultado != $this->url) {
       $this->url = $Resultado;
       $this->provider = "YOUTUBE";

       /*$validator = $this->get('validator');
       $errors = $validator->validate($this);
       if (count($errors) == 0) {*/
          return true;
      /*}*/
    }


    // ou é VIMEO?
    $regexstr = '~
     # Match Vimeo link and embed code
    (?:<iframe [^>]*src=")? 	# If iframe match up to first quote of src
    (?:				# Group vimeo url
      https?:\/\/		# Either http or https
      (?:[\w]+\.)*		# Optional subdomains
      vimeo\.com		# Match vimeo.com
      (?:[\/\w]*\/videos?)?	# Optional video sub directory this handles groups links also
      \/			# Slash before Id
      ([0-9]+)		# $1: VIDEO_ID is numeric
      [^\s]*			# Not a space
    )				# End group
    "?				# Match end quote if part of src
    (?:[^>]*></iframe>)?		# Match the end of the iframe
    (?:<p>.*</p>)?		        # Match any title information stuff
    ~ix';

    $iframestr = '$1';

    $Resultado=preg_replace($regexstr, $iframestr, $this->url);
    // preg_replace: si se cumplen las reglas devuelve el código del vídeo
    //                sino devuelve la url de entrada

    if ($Resultado != $this->url) {
       $this->url = $Resultado;
       $this->provider = "VIMEO";
       return true;
    }

  // SI A URL NON É DE VIMEO OU YOUTUBE
    return false;
  }


  public function sendVideoInscriptionEmail($video)
  {
      $message = \Swift_Message::newInstance()
          ->setSubject('Olloboi 2016. Novo Vídeo Inscrito! :)')
          ->setFrom('info@olloboi.com')
          ->setTo('cosmena@gmail.com')    // ******* Correo electrónico del Usuario Identificado. username_email
          ->setBody(                          // ******* Enviar tamén a info@olloboi.com e a responsableEmailCentro
              $this->renderView(
                  // app/Resources/views/Emails/registration.html.twig
                  'email/video_inscrito.html.twig',
                  array('name' => $name)      // ******* Vídeo inscrito: Datos del vídeo $video-
              ),
              'text/html'
          )
          /*
           * If you also want to include a plaintext version of the message
          ->addPart(
              $this->renderView(
                  'email/video_inscrito.txt.twig',
                  array('name' => $name)
              ),
              'text/plain'
          )*/
      ;
      $this->get('mailer')->send($message);

      // return $this->render(...);
  }



}
