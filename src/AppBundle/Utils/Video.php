<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/Video.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VideoRepository")
 * @ORM\Table(name="video")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"url"},
 *     message="A Url introducida xa foi inscrita no concurso!"
 * )
 */
class Video
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Choice(callback = "getStatuses")
     */
    protected $status;

    public static function getStatuses()
    {
        return array("INSCRITO", "ACEPTADO", "PENDENTE REVISIÓN", "REXEITADO");
    }

    /**
     * @ORM\Column(type="text")
     */
    protected $credits;


    // PROVEEDOR: youtube ou vimeo
    //****************************
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $provider;


    // UN VÍDEO TEN ASOCIADO O USUARIO QUE O ENVIOU
    //*********************************************
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="username_id", referencedColumnName="id")
     */
    protected $username;

    // UN VÍDEO TEN ASOCIADO UN NIVEL EDUCATIVO AO QUE PERTENCE O ALUMNADO QUE PARTICIPOU NEL
    //***************************************************************************************
//    * @ORM\ManyToOne(targetEntity="EducationLevel", inversedBy="name")

    /**
     * @ORM\ManyToOne(targetEntity="EducationLevel")
     * @ORM\JoinColumn(name="education_level_id", referencedColumnName="id")
     */
    // one video has an education level
    protected $educationLevel;

    // UN VÍDEO TEN ASOCIADO UN CENTRO EDUCATIVO DE REFERENCIA
    //********************************************************
    //     * @ORM\ManyToOne(targetEntity="School", inversedBy="school")
    //     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
    /**
     * @ORM\ManyToOne(targetEntity="School")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     */
    // Un vídeo está asociado a un centro de referencia.
    protected $school;


    /**
     * @ORM\Column(type="boolean")
     */
    // marcar obligatoriamente
//    protected $aceptacionBases;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $registrationDateTime;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     *
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set credits
     *
     * @param string $credits
     *
     * @return Product
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
    * Set educationLevel
    *
    * @param \AppBundle\Entity\EducationLevel $educationLevel
    *
    * @return Video
    */
//    public function setEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel)
    public function setEducationLevel($educationLevel)
   {
       $this->educationLevel = $educationLevel;

       return $this;
   }

   /**
    * Get educationLevel
    *
    * @return \AppBundle\Entity\EducationLevel
    */
   public function getEducationLevel()
   {
       return $this->educationLevel;
   }


    /**
     * Set comments
     *
     * @param string $comments
     * @return Video
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set registrationDateTime
     *
     * @param \DateTime $registrationDateTime
     * @return Video
     */
    public function setRegistrationDateTime($registrationDateTime)
    {
        $this->registrationDateTime = $registrationDateTime;

        return $this;
    }

    /**
     * Get registrationDateTime
     *
     * @return \DateTime
     */
    public function getRegistrationDateTime()
    {
        return $this->registrationDateTime;
    }


    /**
     * Set provider
     *
     * @param string $provider
     * @return Video
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }


    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return Video
     */
//     public function setSchool(\AppBundle\Entity\School $school = null)
     public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }


    public function sendVideoInscriptionEmail($video)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Olloboi 2016. Novo Vídeo Inscrito! :)')
            ->setFrom('info@olloboi.com')
            ->setTo('cosmena@gmail.com')    // ******* Correo electrónico del Usuario Identificado. username_email
            ->setBody(                          // ******* Enviar tamén a info@olloboi.com e a responsableEmailCentro
                $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                    'email/video_inscrito.html.twig',
                    array('name' => $name)      // ******* Vídeo inscrito: Datos del vídeo $video-
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'email/video_inscrito.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )*/
        ;
        $this->get('mailer')->send($message);

        // return $this->render(...);
    }






    /**
     * Set status
     *
     * @param string $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set username
     *
     * @param \AppBundle\Entity\User $username
     * @return Video
     */
    public function setUsername(\AppBundle\Entity\User $username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsername()
    {
        return $this->username;
    }
}
