<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/AwardArchive.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AwardArchiveRepository")
 * @ORM\Table(name="awardArchive")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="O nome de categoría debe ser único!"
 * )
 */

class AwardArchive
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

     /**
      * @ORM\Column(type="text")
      */
    protected $description;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $jury;

    // EDICIÓN: 2010, 2011, 2012, etc.
    //****************************
    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $edition;

    /**
     * @ORM\ManyToOne(targetEntity="VideoArchive")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    // Vídeo Seleccionado
    protected $video;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AwardArchive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AwardArchive
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set jury
     *
     * @param string $jury
     * @return AwardArchive
     */
    public function setJury($jury)
    {
        $this->jury = $jury;

        return $this;
    }

    /**
     * Get jury
     *
     * @return string
     */
    public function getJury()
    {
        return $this->jury;
    }

    /**
     * Set edition
     *
     * @param string $edition
     * @return AwardArchive
     */
    public function setEdition($edition)
    {
        $this->edition = $edition;

        return $this;
    }

    /**
     * Get edition
     *
     * @return string
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * Set video
     *
     * @param \AppBundle\Entity\VideoArchive $video
     * @return AwardArchive
     */
    public function setVideo(\AppBundle\Entity\VideoArchive $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \AppBundle\Entity\VideoArchive
     */
    public function getVideo()
    {
        return $this->video;
    }
}
