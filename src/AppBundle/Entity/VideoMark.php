<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VideoMark
 *
 * @ORM\Table(name="video_mark")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VideoMarkRepository")
 */
class VideoMark
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="marks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="marks")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    private $video;

    /**
     * @var bool
     *
     * @ORM\Column(name="seen", type="boolean", nullable=true)
     */
    private $seen;

    /**
     * @var bool
     *
     * @ORM\Column(name="liked", type="boolean", nullable=true)
     */
    private $liked;

    /**
     * @var bool
     *
     * @ORM\Column(name="loved", type="boolean", nullable=true)
     */
    private $loved;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return VideoMark
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set video
     *
     * @param \stdClass $video
     *
     * @return VideoMark
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \stdClass
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set seen
     *
     * @param boolean $seen
     *
     * @return VideoMark
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return bool
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set liked
     *
     * @param boolean $liked
     *
     * @return VideoMark
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;

        return $this;
    }

    /**
     * Get liked
     *
     * @return bool
     */
    public function getLiked()
    {
        return $this->liked;
    }

    /**
     * Set loved
     *
     * @param boolean $loved
     *
     * @return VideoMark
     */
    public function setLoved($loved)
    {
        $this->loved = $loved;

        return $this;
    }

    /**
     * Get loved
     *
     * @return boolean
     */
    public function getLoved()
    {
        return $this->loved;
    }
}
