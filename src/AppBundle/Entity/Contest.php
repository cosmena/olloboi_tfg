<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Contest
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ContestRepository")
 * @ORM\Table(name="contest")
 * @UniqueEntity(
 *     fields={"name"},
 *     message="O nome introducido xa existe na base de datos."
 * )
 */
class Contest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="bases", type="text", nullable=true)
     */
    private $bases;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startRegistration", type="datetime", nullable=true)
     */
    private $startRegistration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endRegistration", type="datetime", nullable=true)
     */
    private $endRegistration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startVoting", type="datetime", nullable=true)
     */
    private $startVoting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endVoting", type="datetime", nullable=true)
     */
    private $endVoting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="awardsDate", type="datetime", nullable=true)
     */
    private $awardsDate;

    /**
     * @var string
     *
     * @ORM\Column(name="awardsSchedule", type="text", nullable=true)
     */
    private $awardsSchedule;

    /**
     * @var string
     *
     * @ORM\Column(name="jury", type="text", nullable=true)
     */
    private $jury;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsors", type="text", nullable=true)
     */
    private $sponsors;

    /**
     * @var string
     *
     * @ORM\Column(name="finalReport", type="text", nullable=true)
     */
    private $finalReport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="published", type="boolean", nullable=false, options={"default":0})
     */
    private $published;


    // UN CONCURSO TEN ASOCIADO UN USUARIO ORGANIZADOR QUE PODE EDITAR A INFORMACIÓN E SELECCIONAR VÍDEOS
    //*************************************************************************************************
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="contests")
     * @ORM\JoinColumn(name="username_id", referencedColumnName="id")
     */
    protected $user;

    // Participants
    /**
     * One Contest can have many participant vídeos.
     * @ORM\OneToMany(targetEntity="Participant", mappedBy="contest", cascade={"remove"})
     */
    private $participants;

    // awards
    /**
     * One Contest can have many awards.
     * @ORM\OneToMany(targetEntity="Award", mappedBy="contest", cascade={"remove"})
     */
    private $awards;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Contest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set bases
     *
     * @param string $bases
     *
     * @return Contest
     */
    public function setBases($bases)
    {
        $this->bases = $bases;

        return $this;
    }

    /**
     * Get bases
     *
     * @return string
     */
    public function getBases()
    {
        return $this->bases;
    }

    /**
     * Set startRegistration
     *
     * @param \DateTime $startRegistration
     *
     * @return Contest
     */
    public function setStartRegistration($startRegistration)
    {
        $this->startRegistration = $startRegistration;

        return $this;
    }

    /**
     * Get startRegistration
     *
     * @return \DateTime
     */
    public function getStartRegistration()
    {
        return $this->startRegistration;
    }

    /**
     * Set endRegistration
     *
     * @param \DateTime $endRegistration
     *
     * @return Contest
     */
    public function setEndRegistration($endRegistration)
    {
        $this->endRegistration = $endRegistration;

        return $this;
    }

    /**
     * Get endRegistration
     *
     * @return \DateTime
     */
    public function getEndRegistration()
    {
        return $this->endRegistration;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Contest
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set startVoting
     *
     * @param \DateTime $startVoting
     *
     * @return Contest
     */
    public function setStartVoting($startVoting)
    {
        $this->startVoting = $startVoting;

        return $this;
    }

    /**
     * Get startVoting
     *
     * @return \DateTime
     */
    public function getStartVoting()
    {
        return $this->startVoting;
    }

    /**
     * Set endVoting
     *
     * @param \DateTime $endVoting
     *
     * @return Contest
     */
    public function setEndVoting($endVoting)
    {
        $this->endVoting = $endVoting;

        return $this;
    }

    /**
     * Get endVoting
     *
     * @return \DateTime
     */
    public function getEndVoting()
    {
        return $this->endVoting;
    }

    /**
     * Set awardsDate
     *
     * @param \DateTime $awardsDate
     *
     * @return Contest
     */
    public function setAwardsDate($awardsDate)
    {
        $this->awardsDate = $awardsDate;

        return $this;
    }

    /**
     * Get awardsDate
     *
     * @return \DateTime
     */
    public function getAwardsDate()
    {
        return $this->awardsDate;
    }

    /**
     * Set awardsSchedule
     *
     * @param string $awardsSchedule
     *
     * @return Contest
     */
    public function setAwardsSchedule($awardsSchedule)
    {
        $this->awardsSchedule = $awardsSchedule;

        return $this;
    }

    /**
     * Get awardsSchedule
     *
     * @return string
     */
    public function getAwardsSchedule()
    {
        return $this->awardsSchedule;
    }

    /**
     * Set finalReport
     *
     * @param string $finalReport
     *
     * @return Contest
     */
    public function setFinalReport($finalReport)
    {
        $this->finalReport = $finalReport;

        return $this;
    }

    /**
     * Get finalReport
     *
     * @return string
     */
    public function getFinalReport()
    {
        return $this->finalReport;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Contest
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set jury
     *
     * @param string $jury
     *
     * @return Contest
     */
    public function setJury($jury)
    {
        $this->jury = $jury;

        return $this;
    }

    /**
     * Get jury
     *
     * @return string
     */
    public function getJury()
    {
        return $this->jury;
    }

    /**
     * Set sponsors
     *
     * @param string $sponsors
     *
     * @return Contest
     */
    public function setSponsors($sponsors)
    {
        $this->sponsors = $sponsors;

        return $this;
    }

    /**
     * Get sponsors
     *
     * @return string
     */
    public function getSponsors()
    {
        return $this->sponsors;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add participant
     *
     * @param \AppBundle\Entity\Participant $participant
     *
     * @return Contest
     */
    public function addParticipant(\AppBundle\Entity\Participant $participant)
    {
        $this->participants[] = $participant;

        return $this;
    }

    /**
     * Remove participant
     *
     * @param \AppBundle\Entity\Participant $participant
     */
    public function removeParticipant(\AppBundle\Entity\Participant $participant)
    {
        $this->participants->removeElement($participant);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Contest
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add award
     *
     * @param \AppBundle\Entity\Award $award
     *
     * @return Contest
     */
    public function addAward(\AppBundle\Entity\Award $award)
    {
        $this->awards[] = $award;

        return $this;
    }

    /**
     * Remove award
     *
     * @param \AppBundle\Entity\Award $award
     */
    public function removeAward(\AppBundle\Entity\Award $award)
    {
        $this->awards->removeElement($award);
    }

    /**
     * Get awards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAwards()
    {
        return $this->awards;
    }
}
