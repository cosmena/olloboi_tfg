<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/Award.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use AppBundle\Entity\Video;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="award")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name", "contest"},
 *     message="O nome de categoría xa existe neste concurso!"
 * )
 */

class Award
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

     /**
      * @ORM\Column(type="text", nullable=true)
      */
    protected $description;

    /**
     * Many Awards belong too One Contest.
     * @ORM\ManyToOne(targetEntity="Contest", inversedBy="awards")
     * @ORM\JoinColumn(name="contest_id", referencedColumnName="id", nullable=true)
     */
    private $contest;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $jury;

    /**
     * Many Awards have Many Nominations.
     * @ORM\ManyToMany(targetEntity="Video", mappedBy="nominations")
     */
    private $nominations;


    /**
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="awards")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id", nullable=true)
     */
    protected $video;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /*************************************************************************************************/
    /*************************************************************************************************/
    /*************************************************************************************************/
    public function __construct()
    {
         //$this->em = $entityManager;
         $this->nominations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Award
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Award
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set jury
     *
     * @param string $jury
     * @return Award
     */
    public function setJury($jury)
    {
        $this->jury = $jury;

        return $this;
    }

    /**
     * Get jury
     *
     * @return string
     */
    public function getJury()
    {
        return $this->jury;
    }

    /**
     * Set contest
     *
     * @param \AppBundle\Entity\Contest $contest
     *
     * @return Award
     */
    public function setContest(\AppBundle\Entity\Contest $contest = null)
    {
        $this->contest = $contest;

        return $this;
    }

    /**
     * Get contest
     *
     * @return \AppBundle\Entity\Contest
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Award
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set video
     *
     * @param \AppBundle\Entity\Video $video
     *
     * @return Award
     */
    public function setVideo(\AppBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \AppBundle\Entity\Video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Add nomination
     *
     * @param \AppBundle\Entity\Video $nomination
     *
     * @return Award
     */
    public function addNomination(\AppBundle\Entity\Video $nomination)
    {
        $this->nominations[] = $nomination;

        return $this;
    }

    /**
     * Remove nomination
     *
     * @param \AppBundle\Entity\Video $nomination
     */
    public function removeNomination(\AppBundle\Entity\Video $nomination)
    {
        $this->nominations->removeElement($nomination);
    }

    /**
     * Get nominations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNominations()
    {
        return $this->nominations;
    }
}
