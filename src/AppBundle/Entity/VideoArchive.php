<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/VideoArchive.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VideoArchiveRepository")
 * @ORM\Table(name="videoArchive")
 */
class VideoArchive
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $edition;


    /**
     * @ORM\Column(type="text")
     */
    protected $credits;


    // PROVEEDOR: youtube ou vimeo
    //****************************
    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $provider;


    // UN VÍDEO TEN ASOCIADO O USUARIO QUE O ENVIOU
    //*********************************************
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="username_id", referencedColumnName="id")
     */
    protected $username;

    // UN VÍDEO TEN ASOCIADO UN NIVEL EDUCATIVO AO QUE PERTENCE O ALUMNADO QUE PARTICIPOU NEL
    //***************************************************************************************
//    * @ORM\ManyToOne(targetEntity="EducationLevel", inversedBy="name")

    /**
     * @ORM\ManyToOne(targetEntity="EducationLevel")
     * @ORM\JoinColumn(name="education_level_id", referencedColumnName="id")
     */
    // one video has an education level
    protected $educationLevel;

    // UN VÍDEO TEN ASOCIADO UN CENTRO EDUCATIVO DE REFERENCIA
    //********************************************************
    //     * @ORM\ManyToOne(targetEntity="School", inversedBy="school")
    //     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
    /**
     * @ORM\ManyToOne(targetEntity="School")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     */
    // Un vídeo está asociado a un centro de referencia.
    protected $school;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $registrationDateTime;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return VideoArchive
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return VideoArchive
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set edition
     *
     * @param string $edition
     * @return VideoArchive
     */
    public function setEdition($edition)
    {
        $this->edition = $edition;

        return $this;
    }

    /**
     * Get edition
     *
     * @return string
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * Set credits
     *
     * @param string $credits
     * @return VideoArchive
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * Set provider
     *
     * @param string $provider
     * @return VideoArchive
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return VideoArchive
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set registrationDateTime
     *
     * @param \DateTime $registrationDateTime
     * @return VideoArchive
     */
    public function setRegistrationDateTime($registrationDateTime)
    {
        $this->registrationDateTime = $registrationDateTime;

        return $this;
    }

    /**
     * Get registrationDateTime
     *
     * @return \DateTime
     */
    public function getRegistrationDateTime()
    {
        return $this->registrationDateTime;
    }

    /**
     * Set username
     *
     * @param \AppBundle\Entity\User $username
     * @return VideoArchive
     */
    public function setUsername(\AppBundle\Entity\User $username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set educationLevel
     *
     * @param \AppBundle\Entity\EducationLevel $educationLevel
     * @return VideoArchive
     */
    public function setEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel = null)
    {
        $this->educationLevel = $educationLevel;

        return $this;
    }

    /**
     * Get educationLevel
     *
     * @return \AppBundle\Entity\EducationLevel
     */
    public function getEducationLevel()
    {
        return $this->educationLevel;
    }

    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return VideoArchive
     */
    public function setSchool(\AppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }
}
