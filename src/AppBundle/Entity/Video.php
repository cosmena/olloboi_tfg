<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/Video.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use \DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use AppBundle\Entity\Award;
use AppBundle\Entity\School;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VideoRepository")
 * @ORM\Table(name="video")
 * @Assert\GroupSequence({"Video", "BD", "VideoInfo"})
 * @UniqueEntity(
 *     fields={"urlId"},
 *     message="A Url introducida xa existe na base de datos."
 * )
 */
// SE NON É ÚNICO => PROPORCIONAR MENSAXE E URL Á FICHA DO VÍDEO.

class Video
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $url;

    // PROVEEDOR: youtube ou vimeo
    //****************************
    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $provider;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $urlId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $proUploadDate;

    /**
     * @ORM\Column(type="integer")
     */
    protected $duration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $urlThumbnail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $urlThumbnailMedium;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $urlThumbnailHigh;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Choice(callback = "getStatuses")
     */
    protected $status;

    public static function getStatuses()
    {
        return array("EXISTE", "ENVIADO", "INSCRITO", "ACEPTADO", "PENDENTE REVISIÓN", "REXEITADO", "NON DISPOÑIBLE");
    }

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $registrationDateTime;



    // UN VÍDEO TEN ASOCIADO O USUARIO QUE O ENVIOU OU NULL SE É DESCOÑECIDO, OU SE SE BORROU O USUARIO
    //*************************************************************************************************
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="videos")
     * @ORM\JoinColumn(name="username_id", referencedColumnName="id")
     */
    protected $username;

    // UN VÍDEO TEN ASOCIADO UN NIVEL EDUCATIVO AO QUE PERTENCE O ALUMNADO QUE PARTICIPOU NEL
    //***************************************************************************************
    /**
     * @ORM\ManyToOne(targetEntity="EducationLevel")
     * @ORM\JoinColumn(name="education_level_id", referencedColumnName="id")
     */
    // one video has an education level
    protected $educationLevel;

    // UN VÍDEO TEN ASOCIADO UN CENTRO EDUCATIVO DE REFERENCIA
    //********************************************************
    //     * @ORM\ManyToOne(targetEntity="School", inversedBy="school")
    //     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
    /**
     * @ORM\ManyToOne(targetEntity="School", inversedBy="videos")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     */
    protected $school;


    /**
     * Un vídeo pode ter moitas marcas, unha por usuario.
     * @ORM\OneToMany(targetEntity="VideoMark", mappedBy="video", cascade={"remove"})
     */
    private $marks;


    /**
     * One Video can participate in Many contests.
     * @ORM\OneToMany(targetEntity="Participant", mappedBy="video", cascade={"remove"})
     */
    private $participations;

    /**
     * Many Videos have Many Nominations.
     * @ORM\ManyToMany(targetEntity="Award", inversedBy="nominations")
     * @ORM\JoinTable(name="awards_nominations")
     */
    private $nominations;

    // awards
    /**
     * One Video can have Many Awards.
     * @ORM\OneToMany(targetEntity="Award", mappedBy="video")
     */
    private $awards;






    /*************************************************************************************************/
    /*************************************************************************************************/
    /*************************************************************************************************/

    public function __construct($entityManager)
    {
         $this->em = $entityManager;
         $this->marks = new ArrayCollection();
         $this->participations = new ArrayCollection();
         $this->nominations = new ArrayCollection();
         $this->awards = new ArrayCollection();

    }

    public function sendVideoInscriptionEmail($video)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Olloboi 2016. Novo Vídeo Inscrito! :)')
            ->setFrom('info@olloboi.com')
            ->setTo('cosmena@gmail.com')    // ******* Correo electrónico del Usuario Identificado. username_email
            ->setBody(                          // ******* Enviar tamén a info@olloboi.com e a responsableEmailCentro
                $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                    'email/video_inscrito.html.twig',
                    array('name' => $name)      // ******* Vídeo inscrito: Datos del vídeo $video-
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'email/video_inscrito.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )*/
        ;
        $this->get('mailer')->send($message);

        // return $this->render(...);
    }

    // **************************************************************************
    // COMPROBAR TAMÉN QUE O VÍDEO NON ESTEA XA REXISTRADO NA BASE DE DATOS
    // SE É ASÍ, PROPORCIONAR MENSAXE E URL Á FICHA DO VÍDEO.
    // **************************************************************************
    /**
      * @Assert\IsTrue(message = "Este vídeo xa existe na base de datos.", groups = {"BD"})
      */
      public function isUnique()
      {
        // Busca o vídeo pola id
        $video = $this->em
                ->getRepository('AppBundle:Video')
                ->findByUrlId($this->urlId);

        if ($video) {
            // Erro de validación, o vídeo xa existe na BD
            return false;
        } else {
            return true;
        }
      }


    /**
      * @Assert\IsTrue(message = "A Url introducida non parece de Vimeo ou Youtube.")
      */
    public function isYoutubeOrVimeo()
    {
      // Se estamos na segunda pantalla da inscrición omite esta comprobación
      if ($this->status == "EXISTE") {
        return true;
      } else {
        // Examina a url e comproba se é de Youtube ou de Vimeo.
        //      En Youtube pode fallar nas listas
        //      Garda o Proveedor e o id do vídeo
        //      !!! PROBAR MÁIS CONCIENZUDAMENTE. REVISAR E MELLORAR
        $url = $this->url;

        // é YOUTUBE?
        // Se se trata dunha lista de Youtube
        if ($recorte = strpos($url, "list")) {
          $url = substr($url, 0, $recorte);
        }

        $regexstr = '~
        # Match Youtube link and embed code
        (?:				 # Group to match embed codes
           (?:<iframe [^>]*src=")?	 # If iframe match up to first quote of src
           |(?:				 # Group to match if older embed
              (?:<object .*>)?		 # Match opening Object tag
              (?:<param .*</param>)*     # Match all param tags
              (?:<embed [^>]*src=")?     # Match embed tag to the first quote of src
           )?				 # End older embed code group
        )?				 # End embed code groups
        (?:				 # Group youtube url
           https?:\/\/		         # Either http or https
           (?:[\w]+\.)*		         # Optional subdomains
           (?:               	         # Group host alternatives.
               youtu\.be/      	         # Either youtu.be,
               | youtube\.com		 # or youtube.com
               | youtube-nocookie\.com	 # or youtube-nocookie.com
           )				 # End Host Group
           (?:\S*[^\w\-\s])?       	 # Extra stuff up to VIDEO_ID
           ([\w\-]{11})		         # $1: VIDEO_ID is numeric
           [^\s]*			 # Not a space
        )				 # End group
        "?				 # Match end quote if part of src
        (?:[^>]*>)?			 # Match any extra stuff up to close brace
        (?:				 # Group to match last embed code
           </iframe>		         # Match the end of the iframe
           |</embed></object>	         # or Match the end of the older embed
        )?				 # End Group of last bit of embed code
        ~ix';

        $iframestr = '$1';

        $Resultado=preg_replace($regexstr, $iframestr, $url);
        // preg_replace: si se cumplen las reglas devuelve el código del vídeo
        //                sino devuelve la url de entrada
        if ($Resultado != $url) {
           $this->id = $Resultado;
           $this->urlId = $this->id;
           $this->provider = "YOUTUBE";
           return true;
        }


        // ou é VIMEO?
        $regexstr = '~
         # Match Vimeo link and embed code
        (?:<iframe [^>]*src=")? 	# If iframe match up to first quote of src
        (?:				# Group vimeo url
        	https?:\/\/		# Either http or https
        	(?:[\w]+\.)*		# Optional subdomains
        	vimeo\.com		# Match vimeo.com
        	(?:[\/\w]*\/videos?)?	# Optional video sub directory this handles groups links also
        	\/			# Slash before Id
        	([0-9]+)		# $1: VIDEO_ID is numeric
        	[^\s]*			# Not a space
        )				# End group
        "?				# Match end quote if part of src
        (?:[^>]*></iframe>)?		# Match the end of the iframe
        (?:<p>.*</p>)?		        # Match any title information stuff
        ~ix';

        $iframestr = '$1';

        $Resultado=preg_replace($regexstr, $iframestr, $url);
        // preg_replace: si se cumplen las reglas devuelve el código del vídeo
        //                sino devuelve la url de entrada
        if ($Resultado != $url) {
           $this->id = $Resultado;
           $this->urlId = $this->id;
           $this->provider = "VIMEO";
           return true;
        }

        // SI A URL NON É DE VIMEO OU YOUTUBE
        return false;
      }
    }


    /**
      * @Assert\IsTrue(message = "Non se atopa o vídeo. Pode que estea configurado como privado ou non incrustable. Por favor, revísao.", groups = {"VideoInfo"})
      */
    public function getVideoInfo()
    {
      // Se estamos na segunda pantalla da inscrición omite esta comprobación
      if ($this->status != "EXISTE") {
        if ($this->provider == "VIMEO") {

          $url = "http://vimeo.com/api/v2/video/".$this->id.".xml";

          // COMPROBA SE EXISTE O XML
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_FAILONERROR, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $output = curl_exec ($ch);
          $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close ($ch);
          //echo $info; exit;
          if ($output === false || $info != 200) {
            // Non é posible obter os datos do vídeo
            return false;
          }

          // OBTÉN OS DATOS DO VÍDEO
          $xml = simplexml_load_file($url);

          foreach ($xml->video as $vimeo) {
            if ($vimeo->embed_privacy != "anywhere") {
              // Se non é embebible
              return false;
            }

            $this->url = $vimeo->url;
            $this->title = $vimeo->title;
            $this->description = $vimeo->description;
            //$this->proUploadDate = date_create($vimeo->upload_date);
            $this->proUploadDate = $vimeo->upload_date;
            $this->urlThumbnail = $vimeo->thumbnail_small;
            $this->urlThumbnailMedium = $vimeo->thumbnail_medium;
            $this->urlThumbnailHigh = $vimeo->thumbnail_large;
            $this->duration = $vimeo->duration;
          } // End foreach

        } else {

          // SI ES YOUTUBE
          //APIkey Youtube = AIzaSyCDLp87_TZCal_1qhBJtWcPVPoZslJfJLw
          $url="https://www.googleapis.com/youtube/v3/videos?id=".$this->id."&key=AIzaSyCDLp87_TZCal_1qhBJtWcPVPoZslJfJLw&fields=items(id,snippet(title,publishedAt,description,thumbnails),contentDetails(duration),statistics,status(uploadStatus,privacyStatus,embeddable))&part=snippet,contentDetails,statistics,status";
          //echo $url."</br></br></br>";
          $videoData = json_decode(file_get_contents($url));

          // SE NON HAI DATOS DO VÍDEO
          if (!$videoData->items) {
            // Non é posible obter os datos do vídeo
            return false;
          }
          if ($videoData->items[0]->status->uploadStatus != "processed" ||
              $videoData->items[0]->status->privacyStatus == "private" ||
              !$videoData->items[0]->status->embeddable) {
              // O vídeo aínda non está procesado, ou é privado ou non é embebible
              return false;
          }
          // CONVERTE A DURACIÓN DE YOUTUBE A SEGUNDOS
          preg_match_all("/PT(\d+H)?(\d+M)?(\d+S)?/", $videoData->items[0]->contentDetails->duration, $matches);
          $hours   = strlen($matches[1][0]) == 0 ? 0 : substr($matches[1][0], 0, strlen($matches[1][0]) - 1);
          $minutes = strlen($matches[2][0]) == 0 ? 0 : substr($matches[2][0], 0, strlen($matches[2][0]) - 1);
          $seconds = strlen($matches[3][0]) == 0 ? 0 : substr($matches[3][0], 0, strlen($matches[3][0]) - 1);
          $this->duration = 3600 * $hours + 60 * $minutes + $seconds;
          $this->title = $videoData->items[0]->snippet->title;
          $this->description = $videoData->items[0]->snippet->description;
          //$this->proUploadDate = date_create($videoData->items[0]->snippet->publishedAt);
          $this->proUploadDate = $videoData->items[0]->snippet->publishedAt;
          $this->urlThumbnail = $videoData->items[0]->snippet->thumbnails->default->url;
          $this->urlThumbnailMedium = $videoData->items[0]->snippet->thumbnails->medium->url;
          $this->urlThumbnailHigh = $videoData->items[0]->snippet->thumbnails->high->url;
        }
      }
      return true;
    }



    /*************************************************************************************************/
    /*************************************************************************************************/
    /*************************************************************************************************/


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     *
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set credits
     *
     * @param string $credits
     *
     * @return Product
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
    * Set educationLevel
    *
    * @param \AppBundle\Entity\EducationLevel $educationLevel
    *
    * @return Video
    */
//    public function setEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel)
    public function setEducationLevel($educationLevel)
     {
         $this->educationLevel = $educationLevel;

         return $this;
     }

     /**
      * Get educationLevel
      *
      * @return \AppBundle\Entity\EducationLevel
      */
     public function getEducationLevel()
     {
         return $this->educationLevel;
     }


    /**
     * Set comments
     *
     * @param string $comments
     * @return Video
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set registrationDateTime
     *
     * @param \DateTime $registrationDateTime
     * @return Video
     */
    public function setRegistrationDateTime($registrationDateTime)
    {
        $this->registrationDateTime = $registrationDateTime;

        return $this;
    }

    /**
     * Get registrationDateTime
     *
     * @return \DateTime
     */
    public function getRegistrationDateTime()
    {
        return $this->registrationDateTime;
    }


    /**
     * Set provider
     *
     * @param string $provider
     * @return Video
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }


    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return Video
     */
//     public function setSchool(\AppBundle\Entity\School $school = null)
     public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set username
     *
     * @param \AppBundle\Entity\User $username
     * @return Video
     */
    public function setUsername(\AppBundle\Entity\User $username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set urlId
     *
     * @param string $urlId
     *
     * @return Video
     */
    public function setUrlId($urlId)
    {
        $this->urlId = $urlId;

        return $this;
    }

    /**
     * Get urlId
     *
     * @return string
     */
    public function getUrlId()
    {
        return $this->urlId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Video
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set proUploadDate
     *
     * @param \DateTime $proUploadDate
     *
     * @return Video
     */
    public function setProUploadDate($proUploadDate)
    {
        $this->proUploadDate = $proUploadDate;

        return $this;
    }

    /**
     * Get proUploadDate
     *
     * @return \DateTime
     */
    public function getProUploadDate()
    {
        return $this->proUploadDate;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Video
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set urlThumbnail
     *
     * @param string $urlThumbnail
     *
     * @return Video
     */
    public function setUrlThumbnail($urlThumbnail)
    {
        $this->urlThumbnail = $urlThumbnail;

        return $this;
    }

    /**
     * Get urlThumbnail
     *
     * @return string
     */
    public function getUrlThumbnail()
    {
        return $this->urlThumbnail;
    }



    /**
     * Set urlThumbnailMedium
     *
     * @param string $urlThumbnailMedium
     *
     * @return Video
     */
    public function setUrlThumbnailMedium($urlThumbnailMedium)
    {
        $this->urlThumbnailMedium = $urlThumbnailMedium;

        return $this;
    }

    /**
     * Get urlThumbnailMedium
     *
     * @return string
     */
    public function getUrlThumbnailMedium()
    {
        return $this->urlThumbnailMedium;
    }




    /**
     * Set urlThumbnailHigh
     *
     * @param string $urlThumbnailHigh
     *
     * @return Video
     */
    public function setUrlThumbnailHigh($urlThumbnailHigh)
    {
        $this->urlThumbnailHigh = $urlThumbnailHigh;

        return $this;
    }

    /**
     * Get urlThumbnailHigh
     *
     * @return string
     */
    public function getUrlThumbnailHigh()
    {
        return $this->urlThumbnailHigh;
    }



    /**
     * Add award
     *
     * @param \AppBundle\Entity\Award $award
     *
     * @return Video
     */
    public function addAward(\AppBundle\Entity\Award $award)
    {
        $this->awards[] = $award;

        return $this;
    }

    /**
     * Remove award
     *
     * @param \AppBundle\Entity\Award $award
     */
    public function removeAward(\AppBundle\Entity\Award $award)
    {
        $this->awards->removeElement($award);
    }

    /**
     * Get awards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * Add mark
     *
     * @param \AppBundle\Entity\VideoMark $mark
     *
     * @return Video
     */
    public function addMark(\AppBundle\Entity\VideoMark $mark)
    {
        $this->marks[] = $mark;

        return $this;
    }

    /**
     * Remove mark
     *
     * @param \AppBundle\Entity\VideoMark $mark
     */
    public function removeMark(\AppBundle\Entity\VideoMark $mark)
    {
        $this->marks->removeElement($mark);
    }

    /**
     * Get marks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * Add participation
     *
     * @param \AppBundle\Entity\Participant $participation
     *
     * @return Video
     */
    public function addParticipation(\AppBundle\Entity\Participant $participation)
    {
        $this->participation[] = $participation;

        return $this;
    }

    /**
     * Remove participation
     *
     * @param \AppBundle\Entity\Participant $participation
     */
    public function removeParticipation(\AppBundle\Entity\Participant $participation)
    {
        $this->participation->removeElement($participation);
    }

    /**
     * Get participation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipation()
    {
        return $this->participation;
    }

    /**
     * Get participations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * Add nomination
     *
     * @param \AppBundle\Entity\Award $nomination
     *
     * @return Video
     */
    public function addNomination(\AppBundle\Entity\Award $nomination)
    {
        $this->nominations[] = $nomination;

        return $this;
    }

    /**
     * Remove nomination
     *
     * @param \AppBundle\Entity\Award $nomination
     */
    public function removeNomination(\AppBundle\Entity\Award $nomination)
    {
        $this->nominations->removeElement($nomination);
    }

    /**
     * Get nominations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNominations()
    {
        return $this->nominations;
    }
}
