<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ContestRepository
 */
class ContestRepository extends EntityRepository
{

  // Devolve os concursos coa inscrición aberta
  public function findOpen()
  {
    $now = new \DateTime();
    return $this->getEntityManager()
                ->createQuery(
                'SELECT c FROM AppBundle:Contest c WHERE c.endRegistration > :now ORDER BY c.endRegistration ASC'
                )
                ->setParameter('now', $now )
                ->getResult();
  }

    public function findOneByIdJoinedToCategory($id)
        {
            /*$query = $this->getManager()
                ->createQuery(
                    'SELECT p, c FROM AppBundle:Product p
                    JOIN p.category c
                    WHERE p.id = :id'
                )->setParameter('id', $id);

            try {
                return $query->getSingleResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return null;
            }*/
        }
}
