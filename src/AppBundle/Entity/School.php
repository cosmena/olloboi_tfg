<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


// src/AppBundle/Entity/School.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OneToMany;
use AppBundle\Entity\Video;
use AppBundle\Entity\User;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SchoolRepository")
 * @ORM\Table(name="school")
 * @ORM\HasLifecycleCallbacks()
 */
class School
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="User", mappedBy="school")
     */
    protected $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $town;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $province;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $cp;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $coordx;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $coordy;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $ownership;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    protected $concerted;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    protected $dependent;

    /**
      * One Product has Many Features.
      *  @ORM\OneToMany(targetEntity="Video", mappedBy="school")
      */
    private $videos;

    /**
      * One Product has Many Features.
      *  @ORM\OneToMany(targetEntity="User", mappedBy="school")
      */
    private $users;


//******************************************************************************
//***  GETTERS AND SETTERS   ***************************************************
//******************************************************************************
    public function __construct() {
      $this->videos = new ArrayCollection();
      $this->users = new ArrayCollection();
    }

    public function __toString()
    {
        $Cadena=$this->getName()." - ".$this->getTown();
        return $Cadena;
    }


    /**
     * Set id
     *
     * @param integer $id
     * @return School
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return School
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return School
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return School
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return School
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set town
     *
     * @param string $town
     * @return School
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set province
     *
     * @param string $province
     * @return School
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return School
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return School
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return School
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set coordx
     *
     * @param string $coordx
     * @return School
     */
    public function setCoordx($coordx)
    {
        $this->coordx = $coordx;

        return $this;
    }

    /**
     * Get coordx
     *
     * @return string
     */
    public function getCoordx()
    {
        return $this->coordx;
    }

    /**
     * Set coordy
     *
     * @param string $coordy
     * @return School
     */
    public function setCoordy($coordy)
    {
        $this->coordy = $coordy;

        return $this;
    }

    /**
     * Get coordy
     *
     * @return string
     */
    public function getCoordy()
    {
        return $this->coordy;
    }

    /**
     * Set ownership
     *
     * @param string $ownership
     * @return School
     */
    public function setOwnership($ownership)
    {
        $this->ownership = $ownership;

        return $this;
    }

    /**
     * Get ownership
     *
     * @return string
     */
    public function getOwnership()
    {
        return $this->ownership;
    }

    /**
     * Set concerted
     *
     * @param string $concerted
     * @return School
     */
    public function setConcerted($concerted)
    {
        $this->concerted = $concerted;

        return $this;
    }

    /**
     * Get concerted
     *
     * @return string
     */
    public function getConcerted()
    {
        return $this->concerted;
    }

    /**
     * Set dependent
     *
     * @param string $dependent
     * @return School
     */
    public function setDependent($dependent)
    {
        $this->dependent = $dependent;

        return $this;
    }

    /**
     * Get dependent
     *
     * @return string
     */
    public function getDependent()
    {
        return $this->dependent;
    }


    /**
     * Add video
     *
     * @param \AppBundle\Entity\Video $video
     *
     * @return School
     */
    public function addVideo(\AppBundle\Entity\Video $video)
    {
        $this->videos[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \AppBundle\Entity\Video $video
     */
    public function removeVideo(\AppBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return School
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
