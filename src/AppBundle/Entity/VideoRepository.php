<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Entity;
//namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
/**
 * VideoRepository
 */
class VideoRepository extends EntityRepository
{

  public function findLastAccepted($numVideos)
  {
    $dql = "SELECT v FROM AppBundle:Video v
            WHERE v.status = 'ACEPTADO'
            ORDER BY v.registrationDateTime DESC";

    return $this->getEntityManager()
                ->createQuery($dql)
                ->setMaxResults($numVideos)
                ->getResult();
  }

  public function findAwarded()
  {
    $dql = "SELECT v, COUNT(a) AS HIDDEN premios FROM AppBundle:Video v  JOIN v.awards a
            WHERE v.status = 'ACEPTADO'
            GROUP BY a
            ORDER BY premios DESC";

    return $this->getEntityManager()
                ->createQuery($dql)
                ->getResult();
  }



// Obtén un número $limit de vídeos premiados aleatoriamente
  public function findRandomAwarded($limit)
  {
    // Array de vídeos para o resultado
    $videos = array();

    // Obtén o número de premios
    $count = $this->getEntityManager()
                ->createQuery("SELECT COUNT(a) FROM AppBundle:Award a")
                 ->getSingleScalarResult();

    // Selecciona un vídeo distinto en cada iteración
    do {
      $dql = "SELECT v, COUNT(a) AS HIDDEN premios FROM AppBundle:Video v  JOIN v.awards a
              WHERE v.status = 'ACEPTADO'
              GROUP BY a";
      $video=$this->getEntityManager()
                  ->createQuery($dql)
                  ->setFirstResult(rand(0, $count - 1))
                  ->setMaxResults(1)
                  ->getSingleResult();
      if (!in_array($video, $videos)) {
        $videos[] = $video;
      }
    } while (sizeof($videos) < $limit);

    // Devolve o array de vídeos
    return $videos;
  }






  public function getNumVideosAceptados()
  {
    /*$repo = $this   ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Video');*/

    $qb = $this->createQueryBuilder('v');
    $qb->select('COUNT(v.id)');
    $qb->where('v.status = :status');
    $qb->setParameter('status', "ACEPTADO");

    $count = $qb->getQuery()->getSingleScalarResult();

    return $count;
  }






    public function findAllOrderedByName()
    {
        /*return $this->getManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Product p ORDER BY p.name ASC'
            )
            ->getResult();*/
    }

    public function findOneByIdJoinedToCategory($id)
        {
            /*$query = $this->getManager()
                ->createQuery(
                    'SELECT p, c FROM AppBundle:Product p
                    JOIN p.category c
                    WHERE p.id = :id'
                )->setParameter('id', $id);

            try {
                return $query->getSingleResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return null;
            }*/
        }
}
