<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;

use AppBundle\Entity\School;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    protected $web;

    // UN USUARIO PODE INDICAR O SEU CENTRO EDUCATIVO.
    //************************************************
    //* @ORM\ManyToOne(targetEntity="School", inversedBy="id")
    //* @ORM\JoinColumn(name="id", referencedColumnName="id")

    /**
     * @ORM\ManyToOne(targetEntity="School", inversedBy="users")
     */
    protected $school;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $registrationDateTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email
     */
    protected $email2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email
     */
    protected $email3;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="notification", type="boolean", nullable=false, options={"default":1})
     */
    protected $notification;


    // videos
    /**
     * One User can have Many videos
     * @ORM\OneToMany(targetEntity="Video", mappedBy="username")
     */
    private $videos;

    // marks
    /**
     * One User can make Many marks on different vídeos (max one per video).
     * @ORM\OneToMany(targetEntity="VideoMark", mappedBy="user", cascade={"remove"})
     */
    private $marks;

    // Concursos que organiza
    /**
     * One user can organize many contests.
     * @ORM\OneToMany(targetEntity="Contest", mappedBy="user")
     */
    private $contests;
    //*************************** CAMPOS EXTRA DE USUARIOS: Teléfono, URL, Whatsapp, FB, Twitter, Centro, etc...

//******************************************************************************
//******************************************************************************
//******************************************************************************


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set web
     *
     * @param string $web
     * @return User
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set registrationDateTime
     *
     * @param \DateTime $registrationDateTime
     * @return User
     */
    public function setRegistrationDateTime($registrationDateTime)
    {
        $this->registrationDateTime = $registrationDateTime;

        return $this;
    }

    /**
     * Get registrationDateTime
     *
     * @return \DateTime
     */
    public function getRegistrationDateTime()
    {
        return $this->registrationDateTime;
    }

    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return User
     */
    public function setSchool(\AppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set email2
     *
     * @param string $email2
     * @return User
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set email3
     *
     * @param string $email3
     * @return User
     */
    public function setEmail3($email3)
    {
        $this->email3 = $email3;

        return $this;
    }

    /**
     * Get email3
     *
     * @return string
     */
    public function getEmail3()
    {
        return $this->email3;
    }

    /**
     * Set notification
     *
     * @param boolean $notification
     *
     * @return User
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return boolean
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Add mark
     *
     * @param \AppBundle\Entity\VideoMark $mark
     *
     * @return User
     */
    public function addMark(\AppBundle\Entity\VideoMark $mark)
    {
        $this->marks[] = $mark;

        return $this;
    }

    /**
     * Remove mark
     *
     * @param \AppBundle\Entity\VideoMark $mark
     */
    public function removeMark(\AppBundle\Entity\VideoMark $mark)
    {
        $this->marks->removeElement($mark);
    }

    /**
     * Get marks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarks()
    {
        return $this->marks;
    }



    /**
     * Add contest
     *
     * @param \AppBundle\Entity\Contest $contest
     *
     * @return User
     */
    public function addContest(\AppBundle\Entity\Contest $contest)
    {
        $this->contests[] = $contest;

        return $this;
    }

    /**
     * Remove contest
     *
     * @param \AppBundle\Entity\Contest $contest
     */
    public function removeContest(\AppBundle\Entity\Contest $contest)
    {
        $this->contests->removeElement($contest);
    }

    /**
     * Get contests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContests()
    {
        return $this->contests;
    }

    /**
     * Add video
     *
     * @param \AppBundle\Entity\Video $video
     *
     * @return User
     */
    public function addVideo(\AppBundle\Entity\Video $video)
    {
        $this->videos[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \AppBundle\Entity\Video $video
     */
    public function removeVideo(\AppBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }
}
