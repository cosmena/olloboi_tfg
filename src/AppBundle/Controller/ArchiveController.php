<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\VideoArchive;
/*use AppBundle\Form\Type\VideoType;
use AppBundle\Form\Type\VideoEditType;*/

class ArchiveController extends Controller
{
  /**
   * @Route("/arquivo", name="arquivo")
   */
  public function archiveAction(Request $request)
  {
      return $this->render('archive/archive.html.twig');
  }


  /**
   * @Route("/arquivo/videos", name="arquivo_videos")
   */
  public function archiveVideosAction(Request $request)
  {
    $filtros['educationLevel'] = $request->query->get('educationLevel');

    $repository = $this->getDoctrine()->getRepository('AppBundle:EducationLevel');
    $edLevels = $repository->findAll();

    // RECUPERA VIDEOS USANDO DQL
    $em = $this->getDoctrine()->getManager();
    if ($filtros['educationLevel'] == "") {
      $query = $em->createQuery(
        'SELECT v
        FROM AppBundle:VideoArchive v
        ORDER BY v.id ASC'
      );
    } elseif ($filtros['educationLevel'] == "null") {
      $query = $em->createQuery(
        'SELECT v
        FROM AppBundle:VideoArchive v
        WHERE v.educationLevel is null
        ORDER BY v.id ASC'
      );
    } else {
      $query = $em->createQuery(
        'SELECT v
        FROM AppBundle:VideoArchive v
        WHERE v.educationLevel = :educationLevel
        ORDER BY v.id ASC'
      )->setParameter('educationLevel', $filtros['educationLevel']);
    }

    // UTILIZA KNP_PAGINATOR PARA PAXINAR OS RESULTADOS
    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      20/*limit per page*/
    );


    // Imprime a vista cos vídeos do arquivo
    return $this->render('archive/videoList.html.twig', array(
                            'pagination' => $pagination,
                            'edLevels' => $edLevels,
                            'filtros' => $filtros,
      ));
  }

  // *********************** AMOSA A LISTA DE PREMIADOS
    /**
     * @Route("/arquivo/premios", name="arquivo_premios")
     */
    public function archiveAwardsAction(Request $request)
    {
      $filtros['edition'] = $request->query->get('edition');

      $em = $this->getDoctrine()->getManager();

      $query = $em->createQuery('SELECT DISTINCT v.edition FROM AppBundle:AwardArchive v');
      $editions = $query->getResult(); // array

      if ($filtros['edition'] == "") {
        $query = $em->createQuery(
          'SELECT v
          FROM AppBundle:AwardArchive v
          ORDER BY v.id ASC'
        );
      } else {
        $query = $em->createQuery(
          'SELECT v
          FROM AppBundle:AwardArchive v
          WHERE v.edition = :edition
          ORDER BY v.id ASC'
        )->setParameter('edition', $filtros['edition']);
      }

      // UTILIZA KNP_PAGINATOR PARA PAXINAR OS RESULTADOS
      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        10/*limit per page*/
      );

      // Imprime a vista cos vídeos do arquivo
      return $this->render('archive/awardList.html.twig', array(
                              'pagination' => $pagination,
                              'filtros' => $filtros,
                              'editions' => $editions,
                            ));
    }


    // *********************** AMOSA UN VIDEO COA ID DADA
    /**
     * @Route("/arquivo/ver/{videoId}", name="arquivo_ver")
     */
    public function archiveShowArchive($videoId)
    {
      // Busca o vídeo pola id
      $video = $this->getDoctrine()
                ->getRepository('AppBundle:VideoArchive')
                ->find($videoId);

      if (!$video) {
          throw $this->createNotFoundException(
              'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
          );};

      return $this->render('archive/videoShow.html.twig', array("video" => $video, ));
      }




}
