<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;


use AppBundle\Entity\Contest;
use AppBundle\Entity\Award;
use AppBundle\Entity\Participant;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Form\Type\ContestType;
use AppBundle\Form\Type\AdminContestType;
use AppBundle\Form\Type\AwardType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ContestController extends Controller
{

  /**
   * @Route("concursos", name="concursos")
   */
  public function listContestsAction(Request $request)
  {
    // LISTA OS CONCURSOS PUBLICADOS
    $repository = $this->getDoctrine()
              ->getRepository('AppBundle:Contest');

    $contests = $repository->findBy(
      array('published' => 1),
      array('endRegistration' => 'DESC')
    );

    return $this->render('contest/list.html.twig', array(
                            'contests' => $contests
      ));
  }



  /**
   * @Route("concursos/novo/", name="concurso_novo")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function newContestAction(Request $request)
  {
    // CREA UN NOVO OBXECTO DE CONCURSO
    $contest = new Contest();
    // INICIALIZA CAMPOS
    $contest->setPublished(false);

    // CREA O FORUMULARIO
    $form = $this->createForm(ContestType::class, $contest, array('role' => "ROLE_ADMIN"));

    // XESTIONA O FORMULARIO
    $form->handleRequest($request);

    // SE SE ENVIOU O FORMULARIO E É VALIDO
    if ($form->isSubmitted() && $form->isValid()) {
      $contest = $form->getData();
      // GÁRDASE O NOVO CONCURSO NA BD
      $em = $this->getDoctrine()->getManager();
      $em->persist($contest);
      $em->flush();
      // REDIRIXE Á VISTA DO CONCURSO
      return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
    }

    // AMOSA A VISTA DE NOVO CONCURSO
    return $this->render('contest/new.html.twig', array(
        'form' => $form->createView(),
    ));
  }



  /**
   * @Route("concursos/{contestId}/ver", name="concurso_ver")
   */
  public function showContestAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // SE O CONCURSO NON ESTÁ PUBLICADO E NON SE TEN ACCESO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest->getPublished() &&
        !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') &&
        (!$this->getUser() || $this->getUser() != $contest->getUser())) {
          throw $this->createAccessDeniedException(
              'Sentímolo, o concurso non é público aínda e non tes acceso');
    };

    // FORMULARIO PARA ENGADIR UN PREMIO AO CONCURSO
    $award = new Award();
    $award->setContest($contest);

    $form = $this->createForm(AwardType::class, $award);

    $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // SE SE ENVIOU UN FORMULARIO VÁLIDO
            $award = $form->getData();
            // GÁRDASE O NOVO PREMIO NA BD
            $em = $this->getDoctrine()->getManager();
            $em->persist($award);
            $em->flush();
            // REDIRIXE Á VISTA DO CONCURSO
            return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
        }

    // AMOSA A VISTA DO CONCURSO
    return $this->render('contest/show.html.twig', array(
      "contest" => $contest,
      'form' => $form->createView(),
    ));
  }


  /**
   * @Route("concursos/{contestId}/editar", name="concurso_editar")
   * @Security("has_role('ROLE_USER')")
   */
  public function editContestAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // Comproba se é o usuario organizador ou se é un administrador
    if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
      $role = "ROLE_ADMIN";
    } elseif ($this->getUser() == $contest->getUser()) {
      $role = "";
    } else {
      throw $this->createAccessDeniedException(
          'Sentímolo, o concurso non é público aínda e non tes acceso');
    }
    // O formulario será diferente segundo o rol do usuario
    $form = $this->createForm(ContestType::class, $contest, array('role' => $role));

    // XESTIONA O FORMULARIO
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $contest = $form->getData();
      // GÁRDANSE OS CAMBIOS DO CONCURSO NA BD
      $em = $this->getDoctrine()->getManager();
      $em->persist($contest);
      $em->flush();
      // REDIRIXE Á VISTA DO CONCURSO
      return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
    }

    // AMOSA A VISTA DE EDICIÓN DO CONCURSO
    return $this->render('contest/edit.html.twig', array(
        'form' => $form->createView(),
        "contest" => $contest,
    ));
  }



  /**
   * @Route("concursos/{contestId}/borrar", name="concurso_borrar")
   * @Security("has_role('ROLE_USER')")
   */
  public function deleteContestAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);
    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // Comproba se é o usuario organizador ou se é un administrador
    if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
      $role = "ROLE_ADMIN";
    } elseif ($this->getUser() == $contest->getUser()) {
      $role = "";
    } else {
      throw $this->createAccessDeniedException(
          'Sentímolo, o concurso non é público aínda e non tes acceso');
    }

    // ELIMINA O CONCURSO E ACTUALIZA A BASE DE DATOS
    $em = $this->getDoctrine()->getManager();
    $em->remove($contest);
    $em->flush();

    // REDIRIXE Á VISTA DO CONCURSO
    return $this->redirectToRoute('concursos');
}



/************************   DE/SELECCIONA PARTICIPANTES   *********************/
  /**
   * @Route("concursos/{contestId}/videos/{videoId}/seleccionar", name="concurso_video_seleccionar")
   * @Security("has_role('ROLE_USER')")
   */
  public function selectVideoContestAction(Request $request, $contestId, $videoId)
  {
    $em = $this->getDoctrine()->getManager();
    $contest = $em->getRepository('AppBundle:Contest')->find($contestId);
    $video = $em->getRepository('AppBundle:Video')->find($videoId);

    // CREA UN NOVO OBXECTO DE CONCURSO
    $participant = new Participant();
    $participant->setContest($contest);
    $participant->setVideo($video);
    $participant->setdateAdded(date_create());

    $em->persist($participant);
    $em->flush();

    // REDIRIXE Á VISTA DO VIDEO
    return $this->redirectToRoute('video_ver', array("videoId" => $videoId, ));
  }


  /**
   * @Route("concursos/{contestId}/videos/{videoId}/deseleccionar", name="concurso_video_deseleccionar")
   * @Security("has_role('ROLE_USER')")
   */
  public function deselectVideoContestAction(Request $request, $contestId, $videoId)
  {
    $em = $this->getDoctrine()->getManager();

    $participant = $em->getRepository('AppBundle:Participant')->findOneBy(array('contest' => $contestId, 'video' => $videoId));
    $em->remove($participant);

    $em->flush();

    // REDIRIXE Á VISTA DO VIDEO
    return $this->redirectToRoute('video_ver', array("videoId" => $videoId, ));
  }






/************************   XESTIÓN DE CATEGORÍAS DE PREMIOS    ****************/

  /**
   * @Route("premios/{awardId}/borrar", name="premio_borrar")
   * @Security("has_role('ROLE_USER')")
   */
  public function deleteAwardAction(Request $request, $awardId)
  {
    // BUSCA O PREMIO POLA ID
    $em = $this->getDoctrine()->getManager();
    $award = $em->getRepository('AppBundle:Award')->find($awardId);

    // SE NON SE ATOPA O PREMIO DEVOLVE UNHA EXCEPCIÓN
    if (!$award) {
        throw $this->createNotFoundException('Non existe un premio con ese id: '.$awardId);
    };

    $contest = $award->getContest();

    // ELIMINA O PREMIO E ACTUALIZA A BASE DE DATOS
    $em->remove($award);
    $em->flush();

    // REDIRIXE Á VISTA DO CONCURSO
    return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
  }


  /**
   * @Route("premios/{awardId}/editar", name="premio_editar")
   * @Security("has_role('ROLE_USER')")
   */
  public function editAwardAction(Request $request, $awardId)
  {
    // BUSCA O PREMIO POLA ID
    $em = $this->getDoctrine()->getManager();
    $award = $em->getRepository('AppBundle:Award')->find($awardId);

    // SE NON SE ATOPA O PREMIO DEVOLVE UNHA EXCEPCIÓN
    if (!$award) {
        throw $this->createNotFoundException('Non existe un premio con ese id: '.$awardId);
    };

    $contest = $award->getContest();

    // Comproba se é o usuario organizador do concurso ou se é un administrador
    if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
      $role = "ROLE_ADMIN";
    } elseif ($this->getUser() == $contest->getUser()) {
      $role = "";
    } else {
      throw $this->createAccessDeniedException(
          'Sentímolo, o concurso non é público aínda e non tes acceso');
    }

    // CREA E XESTIONA O FORMULARIO DE EDICIÓN DE CATEGORÍA
    $form = $this->createForm(AwardType::class, $award);
    // XESTIONA O FORMULARIO
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $award = $form->getData();
      // GÁRDANSE OS CAMBIOS DO CONCURSO NA BD
      $em->persist($contest);
      $em->flush();
      // REDIRIXE Á VISTA DO CONCURSO
      return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
    }

    // CREA E XESTIONA O FORMULARIO DE NOMINADOS
    // OBTÉN OS VÍDEOS SELECCIONADOS NO CONCURSO
    $repository = $this->getDoctrine()->getRepository('AppBundle:Participant');
    $participants = $repository->findByContest($contest->getId());
    /****************  SE NON HAI VÍDEOS ... EXIT? **************/

    // FORMULARIO PARA ENGADIR NOMINADO Á CATEGORÍA
    $fb = $this->createFormBuilder();
    $fb->add('NovoNominado', ChoiceType::class, array(
                              'label'  => 'Nova nominación',
                              //'required' => false,
                              'choices' => $participants,
                              'choice_label' => function($participant, $key, $index) {
                                  return $participant->getVideo()->getTitle();},
                            ));

    $formNom = $fb->getForm();

    // XESTIONA O FORMULARIO
    $formNom->handleRequest($request);
        if ($formNom->isSubmitted() && $formNom->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // REVISAR: CÓDIGO fUNCIONAL PERO MOI ChUNGO
            $nominations = $formNom->getData();
            foreach ($nominations as &$nomination) {
            }

            $video=$nomination->getVideo();

            $XaNominado = false;
            foreach ($award->getNominations() as &$NominatedVideo) {
              if ($NominatedVideo == $video)
                $XaNominado = true;
            }

            if (!$XaNominado) {
              // Se o vídeo non está xa nominado, engade a nominación
              $award->addNomination($video);
              $video->addNomination($award);

              $em->persist($award);
              $em->persist($video);
              $em->flush();
              // REDIRIXE Á VISTA DO CONCURSO
              return $this->redirectToRoute('premio_editar', array("awardId" => $award->getId(), ));
            } else {
              // Se o vídeo xa estaba nominado?
            }
        }

        // CREA E XESTIONA O FORMULARIO DE GAÑADOR
        // OBTÉN OS VÍDEOS NOMINADOS NA CATEGORÍA
        $nominado = [];
        foreach ($award->getNominations() as &$nomination) {
            $nominado[$nomination->getTitle()] = $nomination->getId();
        }
        /****************  SE NON HAI VÍDEOS ... EXIT? **************/

        // FORMULARIO PARA ENGADIR NOMINADO Á CATEGORÍA
        if ($award->getVideo()) {
          $data = $award->getVideo()->getId();
        } else {
          $data = null;
        }

        $fb = $this->createFormBuilder();
        $fb->add('winner', ChoiceType::class, array(
                                  'label'  => 'Vídeo gañador',
                                  'required' => false,
                                  'choices' => $nominado,
                                  'data' => $data,
                                ));

        $formWin = $fb->getForm();
        // XESTIONA O FORMULARIO
        $formWin->handleRequest($request);
            if ($formWin->isSubmitted() && $formWin->isValid()) {
                $em = $this->getDoctrine()->getManager();
                // REVISAR: CÓDIGO fUNCIONAL PERO MOI ChUNGO
                $winners = $formWin->getData();
                foreach ($winners as &$winner) {
                }
                $videoId = $winner;
                if ($videoId) {
                  $video=$this->getDoctrine()->getRepository('AppBundle:Video')
                              ->find($videoId);
                } else {
                  $video = null;
                }

                $award->setVideo($video);
                $em->persist($award);
                $em->flush();
                // REDIRIXE Á VISTA DA CATEGORÍA
                return $this->redirectToRoute('premio_editar', array("awardId" => $award->getId(), ));
            }

    // AMOSA A VISTA DE EDICIÓN DO CONCURSO
    return $this->render('contest/awardEdit.html.twig', array(
      'form' => $form->createView(),
      'formNom' => $formNom->createView(),
      'formWin' => $formWin->createView(),
      'award' => $award,
    ));

  }



/************************   XESTIONA NOMINADOS           *********************/

  /**
   * @Route("nominacions/{awardId}/{videoId}/borrar", name="nominacion_borrar")
   * @Security("has_role('ROLE_USER')")
   */
  public function deletenominationAction(Request $request, $awardId, $videoId)
  {
    // BUSCA O PREMIO POLA ID
    $em = $this->getDoctrine()->getManager();
    $award = $em->getRepository('AppBundle:Award')
             ->find($awardId);
    $video = $em->getRepository('AppBundle:Video')
             ->find($videoId);

    // SE NON SE ATOPA O PREMIO DEVOLVE UNHA EXCEPCIÓN
    if (!$award) {
        throw $this->createNotFoundException('Non existe unha nominación para o vídeo '.$videoId.' para o premio '.$awardId);
    };

    // ELIMINA O PREMIO E ACTUALIZA A BASE DE DATOS
    $award->removeNomination($video);
    $video->removeNomination($award);
    $em->flush();

    // REDIRIXE Á VISTA DO CONCURSO
    return $this->redirectToRoute('premio_editar', array("awardId" => $award->getId(), ));
  }








/************************   ASIGNAR GAÑADORES DE PREMIOS  *********************/
/**
 * @Route("concursos/{contestId}/asignarpremios", name="concurso_asignar_premios")
 * @Security("has_role('ROLE_USER')")
 */
public function ContestAssignAwardsAction(Request $request, $contestId)
{
  // BUSCA O CONCURSO POLA ID
  $contest = $this->getDoctrine()
            ->getRepository('AppBundle:Contest')
            ->find($contestId);

  // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
  if (!$contest) {
      throw $this->createNotFoundException(
          'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
      );};

  // OBTÉN OS PREMIOS ASOCIADOS AO CONCURSO
  $repository = $this->getDoctrine()->getRepository('AppBundle:Award');
  $awards = $repository->findByContest($contestId);
  /****************  SE NON HAI PREMIOS ... EXIT? **************/

  // FORMULARIO PARA ASIGNAR PREMIOS AO CONCURSO
  // CREA O FORMULARIO
  $fb = $this->createFormBuilder();

  foreach ($awards as &$award) {
    // PARA CADA PREMIO CREA UN CAMPO DO FORMULARIO E ASIGNA O VALOR POR DEFECTO
    // OBTÉN OS VÍDEOS NOMINADOS NA CATEGORÍA
    $nominado = [];
    foreach ($award->getNominations() as &$nomination) {
        $nominado[$nomination->getTitle()] = $nomination->getId();
    }

    if ($award->getVideo()) {
      // SE XA TEN UN VÍDEO ASIGNADO.
      $data = $award->getVideo()->getId();
    } else {
      $data = null;
    }

    $fb->add($award->getId(), ChoiceType::class, array(
                            'label'  => $award->getName(),
                            'required' => false,
                            'choices' => $nominado,
                            'data' => $data,
                          ));
  }

  $form = $fb->getForm();
  // XESTIONA O FORMULARIO
  $form->handleRequest($request);
  if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
          $i=0;
          $winners = $form->getData();
          foreach ($winners as &$videoId) {
            if ($videoId) {
              $video=$this->getDoctrine()->getRepository('AppBundle:Video')
                          ->find($videoId);
              $awards[$i]->setVideo($video);
              //echo $winner->getVideo()->getId()." <br> ";
            } else {
              $awards[$i]->setVideo(null);
              //echo "NULL <br> ";
            }
            $i++;
          }
          //exit;
          // GÁRDASE O NOVO PREMIO NA BD
          $em->persist($award);
          $em->flush();
          // REDIRIXE Á VISTA DO CONCURSO
          return $this->redirectToRoute('concurso_ver', array("contestId" => $contest->getId(), ));
      }

  // AMOSA A VISTA DO CONCURSO
  return $this->render('contest/awardsAssign.html.twig', array(
    "contest" => $contest,
    "awards" => $awards,
    'form' => $form->createView(),
  ));
}









/*******************************************************************************
 ****************   VÍDEOS E PREMIOS DO CONCURSO *******************************
 ******************************************************************************/

  /**
   * @Route("concursos/{contestId}/videos", name="concurso_videos")
   */
  public function videosContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // OBTÉN OS TRABALLOS PARTICIPANTES
  /*  $participants = $this->getDoctrine()
              ->getRepository('AppBundle:Participant')
              ->findByContest($contestId);*/

    $filter = $request->query->get('filter');

    $em = $this->getDoctrine()->getManager();

    switch ($filter) {
      case "all":
        $dql = "SELECT p FROM AppBundle:Participant p
                WHERE p.contest = :contestId";
        $query = $em->createQuery($dql)->setParameter('contestId', $contestId);;
        $participants = $query->getResult();
        break;

      case "liked":
        $user = $this->getUser();
        $dql = "SELECT p FROM AppBundle:Participant p
                         JOIN p.video v
                         JOIN v.marks m
                WHERE p.contest = :contestId
                AND m.user = :user
                AND m.liked = 1";

        $query = $em->createQuery($dql)
                    ->setParameter('contestId', $contestId)
                    ->setParameter('user', $user);
        $participants = $query->getResult();
        break;

      case "loved":
        $user = $this->getUser();
        $dql = "SELECT p FROM AppBundle:Participant p
                         JOIN p.video v
                         JOIN v.marks m
                WHERE p.contest = :contestId
                AND m.user = :user
                AND m.loved = 1";

        $query = $em->createQuery($dql)
                    ->setParameter('contestId', $contestId)
                    ->setParameter('user', $user);
        $participants = $query->getResult();
        break;

      case "notseen":
        $user = $this->getUser();
        $dql = "SELECT IDENTITY(m.video)
                FROM AppBundle:VideoMark m
                WHERE m.user = :user
                AND m.seen = 1";
        $query = $em->createQuery($dql)->setParameter('user', $user);
        $seenVideos = $query->getResult();

        $dql = "SELECT p FROM AppBundle:Participant p JOIN p.video v
                WHERE p.contest = :contestId
                AND p.video NOT IN (:seenVideos)";
        $query = $em->createQuery($dql)
                    ->setParameter('contestId', $contestId)
                    ->setParameter('seenVideos', $seenVideos);
        $participants = $query->getResult();

        break;
      case "last":
      default:
        $dql = "SELECT p FROM AppBundle:Participant p JOIN p.video v
                WHERE p.contest = :contestId
                ORDER BY v.registrationDateTime DESC";
        $query = $em->createQuery($dql)
                    ->setParameter('contestId', $contestId)
                    ->setMaxResults( 100 );
        $participants = $query->getResult();
;
    }



    // AMOSA A VISTA DOS VÍDEOS DO CONCURSO
    return $this->render('contest/videos.html.twig', array(
      "contest" => $contest,
      "participants" => $participants,
      'filter' => $filter,
    ));
  }


  /**
   * @Route("concursos/{contestId}/nominados", name="concurso_nominados")
   */
  public function nominationsContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DOS NOMINADOS DO CONCURSO
    return $this->render('contest/nominations.html.twig', array(
      "contest" => $contest,
    ));
  }


  /**
   * @Route("concursos/{contestId}/premios", name="concurso_premios")
   */
  public function awardsContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // OBTÉN OS TRABALLOS PARTICIPANTES
    $awards = $this->getDoctrine()
              ->getRepository('AppBundle:Award')
              ->findByContest($contestId);

    // AMOSA A VISTA DOS PREMIADOS DO CONCURSO
    return $this->render('contest/awards.html.twig', array(
      "contest" => $contest,
    ));
  }


/*******************************************************************************
 ****************   PÁXINAS "ESTÁTICAS": BASES, XURADO, PROGRAMA, ETC. *********
 ******************************************************************************/

  /**
   * @Route("concursos/{contestId}/bases", name="concurso_bases")
   */
  public function basesContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DAS BASES DO CONCURSO
    return $this->render('contest/bases.html.twig', array(
      "contest" => $contest,
    ));
  }

  /**
   * @Route("concursos/{contestId}/programa", name="concurso_programa")
   */
  public function scheduleContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DO PROGRAMA DO CONCURSO
    return $this->render('contest/schedule.html.twig', array(
      "contest" => $contest,
    ));
  }

  /**
   * @Route("concursos/{contestId}/xurado", name="concurso_xurado")
   */
  public function juryContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DO XURADO DO CONCURSO
    return $this->render('contest/jury.html.twig', array(
      "contest" => $contest,
    ));
  }

  /**
   * @Route("concursos/{contestId}/colabora", name="concurso_colabora")
   */
  public function sponsorsContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DOS COLABORADORES DO CONCURSO
    return $this->render('contest/sponsors.html.twig', array(
      "contest" => $contest,
    ));
  }

  /**
   * @Route("concursos/{contestId}/memoria", name="concurso_memoria")
   */
  public function reportContestAwardsAction(Request $request, $contestId)
  {
    // BUSCA O CONCURSO POLA ID
    $contest = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->find($contestId);

    // SE NON SE ATOPA O CONCURSO DEVOLVE UNHA EXCEPCIÓN
    if (!$contest) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un concurso con ese identificador: '.$contestId
        );};

    // AMOSA A VISTA DA MEMORIA FINAL DO CONCURSO
    return $this->render('contest/report.html.twig', array(
      "contest" => $contest,
    ));
  }



}
