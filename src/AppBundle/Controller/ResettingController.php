<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// INICIO-MOD ENGADIDOS
// Para usar anotacións nas rutas
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Para xerar erros novos no formulario
use Symfony\Component\Form\FormError;
// Campos do FORMULARIO
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
// FIN-MOD


/**
 * Controller managing the resetting of the password.
 */
class ResettingController extends Controller
{
    /**
    * Request reset user password: show form.
    */
    /**
    * @Route("/resetting/request", name="fos_user_resetting_request")
    */
    public function requestAction(Request $request)
    {
      $defaultData = array('username' => '', 'email' => '');
      $form = $this->createFormBuilder($defaultData)
          ->add('username', TextType::class, array(
              'label'  => 'Nome de usuari@',
              'required' => false,))
          ->add('email', EmailType::class, array(
              'label'  => 'Correo electrónico',
              'required' => false,))
          ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted()) {
        $username = $form["username"]->getData();
        $email = $form["email"]->getData();

        if (!$username && !$email) {
          // COMPROBA SE SE INTRODUCIRON DATOS NO FORMULARIO
          $error = new FormError("Tes que indicar o teu nome de usuari@ ou o teu correo electrónco.");
          $form->get('username')->addError($error);
        } else {
          if (!$username) {
            // SE SÓ SE INTRODUCÍU O EMAIL
            $query = $this->getDoctrine()->getManager()->createQuery(
              'SELECT u FROM AppBundle:User u
               WHERE u.email = :email OR u.email2 = :email OR u.email3 = :email'
            );
            $result = $query->setParameter('email', $email)
            ->getResult();
          } elseif (!$email) {
            // SE SÓ SE INTRODUCÍU O USUARIO
            $query = $this->getDoctrine()->getManager()->createQuery(
              'SELECT u FROM AppBundle:User u
               WHERE u.usernameCanonical = :usernameC'
            );
            $result = $query->setParameter('usernameC', $username)
            ->getResult();
          } else {
            // SE SE INTRODUCIRON OS DOUS DATOS
            $query = $this->getDoctrine()->getManager()->createQuery(
              'SELECT u FROM AppBundle:User u
               WHERE u.usernameCanonical = :usernameC
               OR (u.email = :email OR u.email2 = :email OR u.email3 = :email)'
            );
            $result = $query->setParameters(array ('email' => $email, 'usernameC' => $username))
            ->getResult();
          }

          if (!$result) {
            // SE NON HAI COINCIDENCIAS NA BD
            $error = new FormError("Non existe ningún usuario na base de datos con eses datos.");
            $form->get('username')->addError($error);
          } elseif (count($result) > 1) {
            // SE HAI MÁIS DUNHA COINCIDENCIA
            $error = new FormError("Os datos introducidos pertencen a máis dun usuario. Proba a encher só un campo.");
            $form->get('username')->addError($error);
          }
        }

        if ($form->isValid()) {
            $username = $result[0]->getEmailCanonical(); //echo $username; exit;
            return new RedirectResponse($this->generateUrl('fos_user_resetting_send_email', array('username' => $username)));
        }
      }

      // RENDERIZA O FORMULARIO
      return $this->render('@FOSUser/Resetting/request.html.twig', array(
                           'form' => $form->createView(),
                           ));
    }


     /**
      * @Route("/resetting/send-email{username}", name="fos_user_resetting_send_email")
      */
    public function sendEmailAction(Request $request, $username)
    {
        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        /* Dispatch init event */
        $event = new GetResponseNullableUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $ttl = $this->container->getParameter('fos_user.resetting.retry_ttl');

        if (null !== $user && !$user->isPasswordRequestNonExpired($ttl)) {
            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_REQUEST, $event);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            if (null === $user->getConfirmationToken()) {
                /** @var $tokenGenerator TokenGeneratorInterface */
                $tokenGenerator = $this->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            /* Dispatch confirm event */
            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_CONFIRM, $event);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
            $user->setPasswordRequestedAt(new \DateTime());
            $this->get('fos_user.user_manager')->updateUser($user);

            /* Dispatch completed event */
            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_SEND_EMAIL_COMPLETED, $event);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
        }

        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('username' => $username)));
    }


     /**
      * @Route("/resetting/check-email", name="fos_user_resetting_check_email")
      */
    public function checkEmailAction(Request $request)
    {
        $username = $request->query->get('username');
        //echo "HOLA".$username; exit;

        if (empty($username)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
        }

        return $this->render('@FOSUser/Resetting/check_email.html.twig', array(
            'tokenLifetime' => ceil($this->container->getParameter('fos_user.resetting.retry_ttl') / 3600),
            'username' => $username,
        ));
    }

    /**
     * Reset user password.
     *
     * @param Request $request
     * @param string  $token
     *
     * @return Response
     */
    public function resetAction(Request $request, $token)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.resetting.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
                FOSUserEvents::RESETTING_RESET_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );

            return $response;
        }

        return $this->render('@FOSUser/Resetting/reset.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }
}
