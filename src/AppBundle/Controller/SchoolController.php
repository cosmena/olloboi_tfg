<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Entity\School;
use AppBundle\Form\Type\SchoolType;

class SchoolController extends Controller
{


  /**
   * @Route("/escolas", name="escolas")
   */
   public function listEscolasAction(Request $request)
   {

     $query = $this->getDoctrine()->getManager()->createQuery(
       'SELECT s FROM AppBundle:School s
       JOIN s.videos v
       WHERE s.id != 0'
     );

     // SE XA EXISTE O email2 => ERRO
     $schools = $query->getResult();

     return $this->render('school/list.html.twig', array(
                             'schools' => $schools
       ));
   }


    // *********************** AMOSA UN CENTRO EDUCATIVO COA ID DADA
    /**
     * @Route("/escolas/{schoolId}", name="escola")
     */
    public function showSchool($schoolId)
    {
      // Busca a escola pola id
    	$school = $this->getDoctrine()
                ->getRepository('AppBundle:School')
                ->find($schoolId);

      if (!$school) {
          throw $this->createNotFoundException(
              'Sentímolo, pero non existe un centro educativo con ese identificador: '.$schoolId
          );};
          return $this->render('school/show.html.twig', array("school" => $school));

      }


    /**
     * @Route("escolas/{schoolId}/editar", name="escola_editar")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editSchoolAction(Request $request, $schoolId)
    {
      // BUSCA POLA ID
      $school = $this->getDoctrine()
                ->getRepository('AppBundle:School')
                ->find($schoolId);

      // SE NON SE ATOPA DEVOLVE UNHA EXCEPCIÓN
      if (!$school) {
          throw $this->createNotFoundException(
              'Sentímolo, pero non existe unha escola con ese identificador: '.$schoolId
          );};

      $form = $this->createForm(SchoolType::class, $school);

      // XESTIONA O FORMULARIO
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $school = $form->getData();
        $em = $this->getDoctrine()->getManager();
        $em->persist($school);
        $em->flush();
        // REDIRIXE Á VISTA DA ESCOLA
        return $this->redirectToRoute('escola_ver', array("schoolId" => $school->getId(), ));
      }

      // AMOSA A VISTA DE EDICIÓN DO CONCURSO
      return $this->render('school/edit.html.twig', array(
          'form' => $form->createView(),
          "contest" => $school,
      ));
    }


  /**
   * @Route("/escolas/{schoolId}/borrar", name="escola_borrar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function deleteSchoolAction(Request $request, $schoolId)
  {
    // BUSCA POLA ID
    $em = $this->getDoctrine()->getManager();
    $school = $em->getRepository('AppBundle:School')->find($schoolId);

    // SE NON SE ATOPA DEVOLVE UNHA EXCEPCIÓN
    if (!$school) {
        throw $this->createNotFoundException('Non existe unha escola con ese id: '.$schoolId);
    };

    $em->remove($school);
    $em->flush();

    // REDIRIXE A...
    return $this->redirectToRoute('escolas');
  }



  // REVISAR: BORRADO DE ELEMENTOS DUNHA DATATABLES, AJAX? CONTROLAR REDIRECCION?...
  /**
   * @Route("admin/escolas/{schoolId}/borrarList", name="admin_list_escola_borrar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function adminListDeleteSchoolAction(Request $request, $schoolId)
  {
    // BUSCA POLA ID
    $em = $this->getDoctrine()->getManager();
    $school = $em->getRepository('AppBundle:School')->find($schoolId);

    // SE NON SE ATOPA O VIDEO DEVOLVE UNHA EXCEPCIÓN
    if (!$school) {
        throw $this->createNotFoundException('Non existe unha escola con ese id: '.$schoolId);
    };

    $em->remove($school);
    $em->flush();

    // REDIRIXE A...
    return $this->redirectToRoute('escolas');
  }


}
