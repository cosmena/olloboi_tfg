<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;

use FOS\UserBundle\Controller\ProfileController as BaseController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

// INICIO-MOD ENGADIDOS
// Para usar anotacións nas rutas
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Para xerar erros novos no formulario
use Symfony\Component\Form\FormError;
// Para poder acceder á entidade Video
use AppBundle\Entity\Video;
// FIN-MOD


class ProfileController extends BaseController
{
  /**
   * @Route("/borra_usuario", name="user_delete")
   */
 public function deleteAction()
 {
       $user = $this->getUser();

       return $this->render('FOSUserBundle:Profile:delete.html.twig', array(
           'user' => $user));

 }


 /**
  * @Route("/usuario_borrado", name="user_delete_confirm")
  */
public function deleteConfirmAction()
{
  $user = $this->getUser();

  // CAMBIAR PROPIETARIO DOS SEUS VIDEOS E CONCURSOS A null
  // Busca vídeos presentados polo usuario
  $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Video');
  $videos = $repository->findByUsername($user);
  // Modifica o usuario dos vídeos atopados, a null
  foreach ($videos as &$video) {
    $video->setUsername(null);
  }

  $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Contest');
  $contests = $repository->findByUser($user);
  // Modifica o usuario dos vídeos atopados, a null
  foreach ($contests as &$contest) {
    $contest->setUser(null);
  }

  // BORRA USUARIO E, EN CASCADA, AS MARCAS DE VÍDEOS QUE FIXERA
  $userManager = $this->get('fos_user.user_manager');
  $userManager->deleteUser($user);


  // ENVIAR CORREO A ADMIN (LOG)
  //***************   ENVIAR CORREO A ADMINISTRADOR
  $message = \Swift_Message::newInstance()
      ->setSubject('Usuari@ Borrad@: '.$user->getUsername())
      ->setFrom(array('info@olloboi.com' => 'Festival Audiovisual Escolar Olloboi'))
      ->setTo('cosmena@gmail.com')    // ******* dirección ADMIN.
      //->setBcc('info@olloboi.com')
      ->setBody(
          $this->renderView(
              'email/user_delete.email.twig',
              array('user' => $user)
          ),
          'text/html'
      );
  $this->get('mailer')->send($message);

  //***********************************************

  // ENVIAR ULTIMO CORREO A USUARIO?


  // SAIR DE SESIÓN
  return $this->redirectToRoute('fos_user_security_logout');
}


  // COMPROBA (POR AJAX) SE SE UTILIZOU UN CORREO SECUNDARIO PARA INICIAR SESIÓN
  // E SE É ASÍ, DEVOLVE O NOME DE USUARIO
  /**
   * @Route("/secondarymails/{mail}", name="secondary_mails")
   */
 public function secondaryMailsAction($mail)
 {
     // BUSCAR MAILS SECUNDARIOS
     $user = $this->getDoctrine()
               ->getRepository('AppBundle:User')
               ->findOneByEmail2($mail);
     if ($user) {
       $username = $user->getUsernameCanonical();
     } else {
       $user = $this->getDoctrine()
                 ->getRepository('AppBundle:User')
                 ->findOneByEmail3($mail);
       if ($user) {
         $username = $user->getUsernameCanonical();
       } else {
         $username = $mail;
       }
     }
    echo $username; exit;
 }


    /**
     * Show the user
     */
     /**
      * @Route("/profile/", name="fos_user_profile_show")
      */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        // BUSCAR VIDEOS PRESENTADOS POLO USUARIO
        $repository = $this->getDoctrine()
                  ->getRepository('AppBundle:Video');
        $videos = $repository->findByUsername($user);
        //if ($videos) {echo "Hay"; exit;} else {echo "No hay";exit;}
        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
            'user' => $user, 'videos' => $videos
        ));
    }

    /**
     * Edit the user
     */
     /**
      * @Route("/profile/edit", name="fos_user_profile_edit")
      */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        // @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        // @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
          // INICIO-MOD
          // COMPARO O email CON email2 E email3;
          $email = $form["email"]->getData();
          $email2 = $form["email2"]->getData();
          $email3 = $form["email3"]->getData();

          if ($email2 == $email || $email3 == $email) {
            // COMPROBA SE OS EMAILS ALTERNATIVOS COINCIDEN CO PRINCIPAL
            $error = new FormError("Os correos electrónicos alternativos non poden ser iguais ao correo principal.");
            $form->get('email')->addError($error);
          } elseif ($email2 && $email2 == $email3) {
            // COMPROBA SE OS EMAILS ALTERNATIVOS TEÑEN VALOR E SON IGUAIS
            $error = new FormError("Os correos electrónicos alternativos non poden ser iguais.");
            $form->get('email2')->addError($error);
          } else {
            // COMPROBA SE OS EMAILS ALTERNATIVOS XA EXISTEN NOUTRO USUARIO DA BASE DE DATOS
            $usernameC = $this->getUser()->getUsernameCanonical();
            $query = $this->getDoctrine()->getManager()->createQuery(
              'SELECT u FROM AppBundle:User u
               WHERE u.usernameCanonical <> :usernameC
               AND (u.email = :email OR u.email2 = :email OR u.email3 = :email)'
            );

            // SE XA EXISTE O email2 => ERRO
            $result = $query->setParameters(array ('email' => $email2, 'usernameC' => $usernameC))
            ->getResult();
            if ($result) {
              $error = new FormError("O correo electrónico alternativo xa está rexistrado na base de datos con outr@ usuari@.");
              $form->get('email2')->addError($error);
            }
            // SE XA EXISTE O email3 => ERRO
            $result = $query->setParameters(array ('email' => $email3, 'usernameC' => $usernameC))
            ->getResult();
            if ($result) {
              $error = new FormError("O correo electrónico alternativo xa está rexistrado na base de datos con outr@ usuari@.");
              $form->get('email3')->addError($error);
            }
          }
          // FIN-MOD

          if ($form->isValid()) {
              // @var $userManager \FOS\UserBundle\Model\UserManagerInterface
              $userManager = $this->get('fos_user.user_manager');

              $event = new FormEvent($form, $request);
              $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

              $userManager->updateUser($user);

              if (null === $response = $event->getResponse()) {
                  $url = $this->generateUrl('fos_user_profile_show');
                  $response = new RedirectResponse($url);
              }

              $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

              return $response;
          }
        }
        return $this->render('FOSUserBundle:Profile:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
