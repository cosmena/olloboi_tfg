<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Form\Type\AdminVideoType;
use AppBundle\Form\Type\VideoEditType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\RedirectResponse;



class AdminController extends Controller
{
  /**
  * @Route("/admin", name="admin")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function adminAction(Request $request)
  {
   // Imprime a plantilla
   return $this->render('admin/admin.html.twig');
  }


  /*******************************************************************************
   ***************    ENVÍO DE CORREOS    *************************************
   ******************************************************************************/

  /**
  * @Route("/admin/correo/confirmado/{message}", name="correo_confirmado")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function mailConfirmedAction(Request $request,$message)
  {

      // RENDERIZA O FORMULARIO
      return $this->render('admin/mail/confirmed.html.twig', array(
                           'message' => $message,
                           ));
  }



  /**
  * @Route("/admin/correo/escribir", name="correo_escribir")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function mailWriteAction(Request $request)
  {
    // CREA O FORMULARIO PARA ENVIAR CORREO
    $defaultData = array('to' => 'Usuarios PROFES');
    $form = $this->createFormBuilder($defaultData)
        ->add('to', TextType::class, array(
            'label'  => 'Para',
            'required' => false,))
        ->add('subject', TextType::class, array(
            'label'  => 'Asunto',
            'required' => false,))
/*        ->add('header', TextareaType::class, array(
            'label'  => 'Encabezado',
            'required' => false,))*/
        ->add('body', TextareaType::class, array(
            'label'  => 'Corpo',
            'required' => false,))
    /*    ->add('footer', TextareaType::class, array(
            'label'  => 'Sinatura',
            'required' => false,))*/
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        if ($form->isValid()) {
                              /*  //***************   ENVIAR CORREO A TEACHERS
                                // Obtén os mails dos usuarios con rol TEACHER
                                $query = $this->getDoctrine()->getManager()
                                            ->createQuery('SELECT u.email FROM AppBundle:User u
                                                           WHERE u.roles LIKE :role')
                                            ->setParameter('role', '%TEACHER%');

                                $result = $query->getResult();
                                for($i = 0; $i < sizeof($result); $i++) {
                                  $emails[] = $result[$i]["email"];
                                }*/

          $emails[] = "cosmena@gmail.com";
          $emails[] = "aguado@edu.xunta.es";
          // Crea a mensaxe
          $message = \Swift_Message::newInstance()
              ->setSubject($form["subject"]->getData())
              ->setFrom(array('info@olloboi.com' => 'Festival Audiovisual Escolar Olloboi'))
              //->setTo("cosmena@gmail.com")    // ******* Correo electrónico destinatario/s
              ->setBcc($emails)  // ** Copia Oculta
              ->setBody(/*$form["header"]->getData().*/$form["body"]->getData()/*.$form["footer"]->getData()*/,
                  'text/html'
              );
          //echo $message; exit;
          // Envía a mensaxe
          $this->get('mailer')->send($message);
          //***************   FIN ENVIAR CORREO
          //***************   GARDAR MENSAXE NA BASE DE DATOS

//          return new RedirectResponse($this->generateUrl('correo_confirmado', array('message' => $message)));
          return $this->render('admin/mail/confirmed.html.twig', array(
                               'message' => $message,
                               ));
        }
      }
      // RENDERIZA O FORMULARIO
      return $this->render('admin/mail/write.html.twig', array(
                           'form' => $form->createView(),
                           ));
  }



  /**
  * @Route("/admin/correo/confirmar", name="correo_confirmar")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function mailConfirmAction(Request $request, $form)
  {
      // RENDERIZA O FORMULARIO
      return $this->render('admin/mail/write.html.twig', array(
                           'form' => $form,
                           ));
  }


  /*******************************************************************************
   ***************    XESTIÓN DE CONCURSOS    *************************************
   ******************************************************************************/


  /**
  * @Route("/admin/concursos", name="admin_concursos")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function adminContestsAction(Request $request)
  {
    $repository = $this->getDoctrine()
                       ->getRepository('AppBundle:Contest');

    $contests = $repository->findBy(
      array(),
      array('endRegistration' => 'DESC')
    );

    return $this->render('admin/adminContests.html.twig', array(
                            'contests' => $contests
      ));
  }



  /*******************************************************************************
   ***************    XESTIÓN DE USUARIOS    *************************************
   ******************************************************************************/

  /**
  * @Route("/admin/usuarios", name="admin_usuarios")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function adminUsersAction(Request $request)
  {
    $user = $this->getUser();

    // Obtén os usuarios
    $userManager = $this->get('fos_user.user_manager');
    $users = $userManager->findUsers();

    // Imprime a plantilla
    return $this->render('admin/adminUsers.html.twig', array(
                            'users' => $users
      ));
  }



  /**
  * @Route("/admin/usuarios/{userId}", name="ver_usuario")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function showUserAction($userId)
  {
    // Busca o usuario pola id
    $user = $this->getDoctrine()
              ->getRepository('AppBundle:User')
              ->find($userId);

    if (!$user) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un usuario con ese identificador: '.$userId
        );};

    return $this->render('admin/user/show.html.twig', array("user" => $user, ));
  }


  /**
  * @Route("/admin/usuarios/{userId}/borrar", name="borrar_usuario")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function deleteUserAction($userId)
  {
    // Busca o usuario pola id
    $user = $this->getDoctrine()
              ->getRepository('AppBundle:User')
              ->find($userId);

    if (!$user) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un usuario con ese identificador: '.$userId
        );};

    // CAMBIAR PROPIETARIO DOS SEUS VIDEOS E CONCURSOS A null
    // Busca vídeos presentados polo usuario
    $repository = $this->getDoctrine()
              ->getRepository('AppBundle:Video');
    $videos = $repository->findByUsername($user);
    // Modifica o usuario dos vídeos atopados, a null
    foreach ($videos as &$video) {
      $video->setUsername(null);
    }

    $repository = $this->getDoctrine()
              ->getRepository('AppBundle:Contest');
    $contests = $repository->findByUser($user);
    // Modifica o usuario dos vídeos atopados, a null
    foreach ($contests as &$contest) {
      $contest->setUser(null);
    }


    // ELIMINA O USUARIO E AS MARCAS EN CASCADA
    // Actualiza a base de datos
    $em = $this->getDoctrine()->getManager();
    $em->remove($user);
    $em->flush();

    return $this->redirectToRoute('admin_usuarios');
  }


  /**
  * @Route("/admin/usuarios/{userId}/rol/{role}", name="rol_usuario")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function roleUserAction($userId, $role)
  {
    // Busca o usuario pola id
    $user = $this->getDoctrine()
              ->getRepository('AppBundle:User')
              ->find($userId);

    if (!$user) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un usuario con ese identificador: '.$userId
        );};

    // Engade ou elimina o rol en función do valor do parámetro
    switch ($role) {
      case "addTeacher":
        $user->addRole("ROLE_TEACHER");
        break;
      case "removeTeacher":
        $user->removeRole("ROLE_TEACHER");
        break;
      case "addAdmin":
        $user->addRole("ROLE_ADMIN");
        break;
      case "removeAdmin":
        $user->removeRole("ROLE_ADMIN");
    };

    // Actualiza a base de datos
    $em = $this->getDoctrine()->getManager();
    $em->flush();

    return $this->redirectToRoute('ver_usuario', array('userId' => $userId, ));
  }



  // *********************** EDITA UN USUARIO COA ID DADA
  /**
   * @Route("/admin/usuarios/edit/{userId}", name="editar_usuario")
   * @Security("has_role('ROLE_ADMIN')")
   */
/*  public function editUserAction($userId)
  {
    // Busca o usuario pola id
  	$user = $this->getDoctrine()
              ->getRepository('AppBundle:User')
              ->find($userId);

    if (!$user) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un usuario con ese identificador: '.$userId
        );};

    // REMATAR NO FUTURO
    return $this->render('admin/user/show.html.twig', array("user" => $user, ));
  }*/






  /*******************************************************************************
   ***************    XESTIÓN DE VÍDEOS    *************************************
   ******************************************************************************/


  /**
  * @Route("/admin/videos", name="admin_videos")
  * @Security("has_role('ROLE_ADMIN')")
  */
  public function adminVideosAction(Request $request)
  {
    $repository = $this->getDoctrine()
              ->getRepository('AppBundle:Video');
    $videos = $repository->findAll();

    return $this->render('admin/adminVideos.html.twig', array(
                            'videos' => $videos
      ));
  }



  /**
   * @Route("/admin/videos/novo/", name="admin_engadir_video")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function AdminEngadirVideoAction(Request $request)
  {
    // CREA UN OBXECTO video. Precisará o Manager de Doctrine para validacións
    $video = new Video($this->getDoctrine()->getManager());
    // Crea o formulario
    $form = $this->createForm(AdminVideoType::class, $video);
    // Xestiona o formulario
    $form->handleRequest($request);

    // SE O FORMULARIO SE ENVIOU E É VÁLIDO...
    if ($form->isSubmitted() && $form->isValid()) {
      // Se o estado está baleiro estamos é que vimos do Paso 1
      if (!$video->getStatus()) {
        // A url é dun vídeo existente, completamos datos do vídeo
        $video->setStatus("EXISTE");
        // Cando o administrador engade o vídeo o usuario vai a 'null'
        $video->setUsername(null);
        // Crea o formulario do Paso 2
        $form = $this->createForm(AdminVideoType::class, $video);
      } else {
        // Os vídeos engadidos polo administrador acéptanse automáticamente
        $video->setStatus("ACEPTADO");
        $date = date_create();
        $video->setRegistrationDateTime ($date);
        $video->setProUploadDate(date_create($video->getProUploadDate()));

        // Todo OK. VAI PARA A BD!
        $em = $this->getDoctrine()->getManager();
        $em->persist($video);
        $em->flush();
        // REDIRIXE Á Vídeo Engadido
        return $this->redirectToRoute('admin_video_added', array('videoId' => $video->getId()));
      }
    }
    // RENDERIZA O FORMULARIO
    return $this->render('admin/video/new.html.twig', array(
                         'form' => $form->createView(),
                         "video" => $video,
                         ));
  }


  /**
   * @Route("/admin/videos/{videoId}/added", name="admin_video_added")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function videoAddedAction(Request $request, $videoId)
  {
    $video = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->find($videoId);

    if (!$video) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
        );};

    return $this->render('admin/video/added.html.twig', array("video" => $video, ));
  }



  /**
   * @Route("/videos/{videoId}/editar", name="video_editar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function editVideoAction(Request $request, $videoId)
  {
    $video = new Video($this->getDoctrine()->getManager());

    // Obtén o vídeo
    $video = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->find($videoId);

    if (!$video) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
        );};

    // Crea o formulario
    $form = $this->createForm(VideoEditType::class, $video);
    // Xestiona o formulario
    $form->handleRequest($request);
    // SE O FORMULARIO SE ENVIOU E É VÁLIDO...
    if ($form->isSubmitted() && $form->isValid()) {
        // Todo OK. VAI PARA A BD!
        $em = $this->getDoctrine()->getManager();
        $em->persist($video);
        $em->flush();
        // REDIRIXE ao detalle do vídeo
        // (para evitar que o usuario poida reenviar o formulario se refresca a páxina)
        return $this->redirectToRoute('video_ver', array('videoId' => $video->getId()));
    }
    // RENDERIZA O FORMULARIO
    return $this->render('video/edit.html.twig', array(
                         'form' => $form->createView(),
                         "video" => $video,
                         ));
  }



  /**
   * @Route("/videos/{videoId}/borrar", name="video_borrar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function deleteVideoAction(Request $request, $videoId)
  {
    // BUSCA O VIDEO POLA ID
    $em = $this->getDoctrine()->getManager();
    $video = $em->getRepository('AppBundle:Video')->find($videoId);

    // SE NON SE ATOPA O VIDEO DEVOLVE UNHA EXCEPCIÓN
    if (!$video) {
        throw $this->createNotFoundException('Non existe un vídeo con ese id: '.$videoId);
    };

    // ELIMINA O VIDEO E ACTUALIZA A BASE DE DATOS
    // As marcas de usuarios e as participacións en concursos bórranse en cascada
    // QUE PASA SE O VÍDEO TEN PREMIOS??
    $em->remove($video);
    $em->flush();

    // REDIRIXE A...
    return $this->redirectToRoute('videos');
  }



  // REVISAR: BORRADO DE ELEMENTOS DUNHA DATATABLES, AJAX? CONTROLAR REDIRECCION?...
  /**
   * @Route("admin/videos/{videoId}/borrarList", name="admin_list_video_borrar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function adminListDeleteVideoAction(Request $request, $videoId)
  {
    // BUSCA O VIDEO POLA ID
    $em = $this->getDoctrine()->getManager();
    $video = $em->getRepository('AppBundle:Video')->find($videoId);

    // SE NON SE ATOPA O VIDEO DEVOLVE UNHA EXCEPCIÓN
    if (!$video) {
        throw $this->createNotFoundException('Non existe un vídeo con ese id: '.$videoId);
    };

    // ELIMINA O VIDEO E ACTUALIZA A BASE DE DATOS
    // As marcas de usuarios e as participacións en concursos bórranse en cascada
    // QUE PASA SE O VÍDEO TEN PREMIOS??
    $em->remove($video);
    $em->flush();

    // REDIRIXE A...
    return $this->redirectToRoute('admin_videos');
  }



}
