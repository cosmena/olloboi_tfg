<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Entity\VideoRepository;
use AppBundle\Entity\Award;
use AppBundle\Entity\VideoMark;
//use AppBundle\Entity\VideoArchive;
use AppBundle\Entity\EducationLevel;
use Doctrine\ORM\EntityRepository;

use AppBundle\Form\Type\VideoType;
use AppBundle\Form\Type\VideoEditType;


class VideoController extends Controller
{

  /**
  * @Route("/videos/{videoId}/ver", name="video_ver")
  */
  public function showVideoAction($videoId)
  {
  $video = $this->getDoctrine()
            ->getRepository('AppBundle:Video')
            ->find($videoId);

  if (!$video) {
      throw $this->createNotFoundException(
          'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
      );};

  return $this->render('video/show.html.twig', array(
                              "video" => $video, ));
  }


  /**
   * @Route("/videos/novo/", name="video_novo")
   * @Security("has_role('ROLE_USER')")
   */
  public function newVideoAction(Request $request)
  {
    // CREA UN OBXECTO video. Precisará o Manager de Doctrine para validacións
    $video = new Video($this->getDoctrine()->getManager());
    // Crea o formulario
    $form = $this->createForm(VideoType::class, $video);
    // Xestiona o formulario
    $form->handleRequest($request);

    // SE O FORMULARIO SE ENVIOU E É VÁLIDO...
    if ($form->isSubmitted() && $form->isValid()) {
      // Obter usuario
      $user = $this->getUser();
      // Se o estado está baleiro estamos é que vimos do Paso 1
      if (!$video->getStatus()) {
        // A url é dun vídeo existente, completamos datos do vídeo
        $video->setStatus("EXISTE");
        $video->setUsername($user);
        $video->setSchool ($user->getSchool());
        // Crea o formulario do Paso 2
        $form = $this->createForm(VideoType::class, $video);
      } else {
        // Vídeo inscrito: completamos últimos datos e insertamos na BD
        $video->setStatus("INSCRITO");
        $date = date_create();
        $video->setRegistrationDateTime ($date);
        $video->setProUploadDate(date_create($video->getProUploadDate()));

        // Todo OK. VAI PARA A BD!
        $em = $this->getDoctrine()->getManager();
        $em->persist($video);
        $em->flush();
        //***************   ENVIAR CORREO A USUARIO (E ADMIN)
        $message = \Swift_Message::newInstance()
            ->setSubject('Novo Vídeo Inscrito: '.$video->getTitle())
            ->setFrom(array('info@olloboi.com' => 'Festival Audiovisual Escolar Olloboi'))
            ->setTo($user->getEmail())    // ******* Correo electrónico del Usuario Identificado.
            ->setBcc('cosmena@gmail.com')  // ** Copia Oculta a ADMIN/ORGANIZERS/etc.
            ->setBody(
                $this->renderView(
                    'email/video_new.email.twig',
                    array('user' => $user, 'video' => $video)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);

        //***************   FIN ENVIAR CORREO

        // REDIRIXE Á Vídeo Inscrito
        // (para evitar que o usuario poida reenviar o formulario se refresca a páxina)
        return $this->redirectToRoute('video_inscrito', array('videoId' => $video->getId()));
      }
    }
    // RENDERIZA O FORMULARIO
    return $this->render('video/new.html.twig', array(
                         'form' => $form->createView(),
                         "video" => $video,
                         ));
  }


  /**
   * @Route("/videos/{videoId}/inscrito", name="video_inscrito")
   * @Security("has_role('ROLE_USER')")
   */
  public function videoInscritoAction(Request $request, $videoId)
  {
    $video = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->find($videoId);

    if (!$video) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
        );};

    return $this->render('video/inscrito.html.twig', array("video" => $video, ));
  }


  /**
   * @Route("/videos/{videoId}/aceptar", name="video_aceptar")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function videoAceptAction(Request $request, $videoId)
  {
    $video = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->find($videoId);

    if (!$video) {
        throw $this->createNotFoundException(
            'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
        );};

    $user = $video->getUsername();

    $video->setStatus("ACEPTADO");
    $em = $this->getDoctrine()->getManager();
    $em->persist($video);
    $em->flush();

    //***************   ENVIAR CORREO A USUARIO (E ADMIN)
    $message = \Swift_Message::newInstance()
        ->setSubject('Vídeo Aceptado! '.$video->getTitle())
        ->setFrom(array('info@olloboi.com' => 'Festival Audiovisual Escolar Olloboi'))
        ->setTo($user->getEmail())    // ******* Correo electrónico del Usuario Identificado.
        ->setBcc('cosmena@gmail.com')  // ** Copia Oculta a ADMIN/ORGANIZERS/etc.
        ->setBody(
            $this->renderView(
                'email/video_acepted.email.twig',
                array('user' => $user, 'video' => $video)
            ),
            'text/html'
        );

    $this->get('mailer')->send($message);

    return $this->redirectToRoute('video_ver', array('videoId' => $video->getId()));
    //***************   FIN ENVIAR CORREO
  }



  /**
   * @Route("/videos/{videoId}/nav/{target}", name="video_navegar")
   */
  public function navigateVideoAction($videoId, $target)
  {
    $em = $this->getDoctrine()->getManager();

    switch($target) {
      case "forward":
          $dql = "SELECT v FROM AppBundle:Video v
                  WHERE v.id >  :currentId
                  ORDER BY v.id";
          $query = $em->createQuery($dql)
                      ->setParameter('currentId', $videoId)
                      ->setMaxResults( 1 );
          $video = $query->getResult();
          break;
      case "backward":
          $dql = "SELECT v FROM AppBundle:Video v
                  WHERE v.id < :currentId
                  ORDER BY v.id DESC";
          $query = $em->createQuery($dql)
                      ->setParameter('currentId', $videoId)
                      ->setMaxResults( 1 );
          $video = $query->getResult();
          break;
      case "random":
          $amount = 1;
          $rows = $em->createQuery('SELECT COUNT(v.id) FROM AppBundle:Video v')->getSingleScalarResult();
          $offset = max(0, rand(0, $rows - $amount - 1));
          $query = $em->createQuery('
                          SELECT DISTINCT v
                          FROM AppBundle:Video v')
          ->setMaxResults($amount)
          ->setFirstResult($offset);

          $video = $query->getResult();
          break;
    };

    /*$video = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->find($videoId);*/
    if ($video) {
      $nextVideoId = $video[0]->getId();
    } else {
        /*throw $this->createNotFoundException(
            'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId);*/
        // ???? Se non hai vídeo anterior ou seguinte amosa o mesmo vídeo.
        $nextVideoId = $videoId;
    };

    return $this->redirectToRoute('video_ver', array('videoId' => $nextVideoId));
    }



  /**
  * @Route("/videos/{videoId}/marcar/{previousMark}", name="video_marcar")
  */
  public function markVideoAction($videoId, $previousMark)
  {
  $user = $this->getUser();

  $em = $this->getDoctrine()->getManager();
  $video = $this->getDoctrine()
            ->getRepository('AppBundle:Video')
            ->find($videoId);

  if (!$video) {
      throw $this->createNotFoundException(
          'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
      );};

  // Obtén as marcas do vídeo se as hai
  $mark = $this->getDoctrine()->getRepository('AppBundle:VideoMark')
               ->findOneBy(array(
                  'user' => $user,
                  'video' => $video));
  if (!$mark) {
    $mark = new VideoMark;
    $mark->setUser($user);
    $mark->setVideo($video);
  }
  switch ($previousMark) {
    case "unliked":
      $mark->setLiked(true);
      $em->persist($mark);
      break;
    case "unloved":
      $mark->setLoved(true);
      $em->persist($mark);
      break;
    case "unseen":
      $mark->setSeen(true);
      $em->persist($mark);
      break;
    case "liked":
      $mark->setLiked(null);
      break;
    case "loved":
      $mark->setLoved(null);
      break;
    case "seen":
      $mark->setSeen(null);
      break;
  }
  if (!$mark->getLiked() &&
      !$mark->getLoved() &&
      !$mark->getSeen()) {
        $em->remove($mark);
      }
  // Todo OK. VAI PARA A BD!
  $em->flush();
  // Nada que devolver por AJAX
  exit;
  //return $this->render('video/show.html.twig', array(
  //  "video" => $video, "mark" => $mark));
  }


  /**
  * @Route("/videos", name="videos")
  */
  public function listVideosAction(Request $request)
  {
  $filter = $request->query->get('filter');

  $em = $this->getDoctrine()->getManager();


  switch ($filter) {
    case "all":
      $dql = "SELECT v FROM AppBundle:Video v
              WHERE v.status = 'ACEPTADO'";
      $query = $em->createQuery($dql);
      $videos = $query->getResult();
      break;

    case "liked":
      $user = $this->getUser();
      $dql = "SELECT v FROM AppBundle:Video v JOIN v.marks m
              WHERE v.status = 'ACEPTADO'
              AND m.user = :user
              AND m.liked = 1";
      $query = $em->createQuery($dql)->setParameter('user', $user);
      $videos = $query->getResult();
      break;

    case "loved":
      $user = $this->getUser();
      $dql = "SELECT v FROM AppBundle:Video v JOIN v.marks m
              WHERE v.status = 'ACEPTADO'
              AND m.user = :user
              AND m.loved = 1";

      $query = $em->createQuery($dql)->setParameter('user', $user);
      $videos = $query->getResult();
      break;

    case "notseen":
      $user = $this->getUser();
      $dql = "SELECT IDENTITY(m.video)
              FROM AppBundle:VideoMark m
              WHERE m.user = :user
              AND m.seen = 1";
      $query = $em->createQuery($dql)->setParameter('user', $user);
      $seenVideos = $query->getResult();

      $dql = "SELECT v FROM AppBundle:Video v
              WHERE v.status = 'ACEPTADO'
              AND v.id NOT IN (:seenVideos)";
      $query = $em->createQuery($dql)->setParameter('seenVideos', $seenVideos);
      $videos = $query->getResult();
      break;

    case "last":
      $videos = $em->getRepository('AppBundle:Video')
                   ->findLastAccepted(100);
      break;

    case "awarded":
    default:
      $videos = $em->getRepository('AppBundle:Video')
                   ->findAwarded();
      break;
  }

   return $this->render('video/list.html.twig', array(
                           'videos' => $videos, 'filter' => $filter
     ));
  }


}
