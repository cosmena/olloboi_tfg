<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/****************  ENGADIDOS PARA LISTADOS */
use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Entity\Contest;
use AppBundle\Entity\Award;
//use AppBundle\Entity\VideoArchive;
use AppBundle\Entity\EducationLevel;
//use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\ContestRepository;
use AppBundle\Entity\VideoRepository;

/****************  FIN ENGADIDOS PARA LISTADOS */





class IndexController extends Controller
{
  /**
   * @Route("/rr", name="index2")
   */
  public function index2Action(Request $request)
  {
    // Vídeos premiados ao azar
    $videos = $this->getDoctrine()
              ->getRepository('AppBundle:Video')
              ->findRandomAwarded(16);

    // Concursos abertos
    $contests = $this->getDoctrine()
              ->getRepository('AppBundle:Contest')
              ->findOpen();

    // Escolas ao azar
    $schools = $this->getDoctrine()
              ->getRepository('AppBundle:School')
              ->findRandom(5);


      return $this->render('index2.html.twig', array(
                              'videos' => $videos,
                              'contests' => $contests,
                              'schools' => $schools));
  }



    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('index.html.twig');
    }


    /**
     * @Route("/olloboi", name="olloboi")
     */
    public function olloboiAction(Request $request)
    {
        return $this->render('index/olloboi.html.twig');
    }

    /**
     * @Route("/prensa", name="prensa")
     */
    public function prensaAction(Request $request)
    {
        return $this->render('index/prensa-e-medios.html.twig');
    }

    /**
     * @Route("/complices", name="complices")
     */
    public function complicesAction(Request $request)
    {
        return $this->render('index/complices.html.twig');
    }

    /**
     * @Route("/crear", name="crear")
     */
    public function crearAction(Request $request)
    {
        return $this->render('index/creadores.html.twig');
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faqAction(Request $request)
    {

      return $this->render('index/faq.html.twig');
    }

    /**
     * @Route("/condicions", name="condicions_uso")
     */
    public function condicionsAction(Request $request)
    {

      return $this->render('index/condicions.html.twig');
    }

}
