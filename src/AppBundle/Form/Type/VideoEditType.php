<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Form/Type/VideoEditType.php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use AppBundle\Entity\Video;


class VideoEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
//          ->add('url', UrlType::class, array('label'  => 'Url *',))
          ->add('title', TextType::class, array('label'  => 'Título *'))
          ->add('educationLevel', EntityType::class, array('class' => 'AppBundle:EducationLevel', 'required' => false))
          ->add('school', EntityType::class, array('label'  => 'Centro Educativo *',
                                  'class' => 'AppBundle:School',
                                  'query_builder' => function (EntityRepository $er) {
                                                     return $er->createQueryBuilder('s')
                                                               ->orderBy('s.name', 'ASC');
                                                     }, ))
          ->add('username', EntityType::class, array('class' => 'AppBundle:User', 'required' => false,
                                  'query_builder' => function (EntityRepository $er) {
                                                     return $er->createQueryBuilder('u')
                                                               ->orderBy('u.username', 'ASC');
                                                     },))
          ->add('description', TextareaType::class, array('label'  => 'Descrición, Sinopse e Créditos', 'required' => false))
          ->add('comments', TextareaType::class, array('label'  => 'Outros comentarios (privados para a organización)',
                                  'required' => false,))
          ->add('status', ChoiceType::class, array(
            'choices' => array("EXISTE" => "EXISTE",
                               "ENVIADO" => "ENVIADO",
                               "INSCRITO" => "INSCRITO",
                               "ACEPTADO" => "ACEPTADO",
                               "PENDENTE REVISIÓN" => "PENDENTE REVISIÓN",
                               "REXEITADO" => "REXEITADO",
                               "NON DISPOÑIBLE" => "NON DISPOÑIBLE")))
/*          ->add('urlId', HiddenType::class)
          ->add('proUploadDate', HiddenType::class)
          ->add('duration', HiddenType::class)
          ->add('urlThumbnail', HiddenType::class)
          ->add('urlThumbnailMedium', HiddenType::class)
          ->add('urlThumbnailHigh', HiddenType::class)
*/
          ;
    }

  public function configureOptions(OptionsResolver $resolver)
	{
    $resolver->setDefaults(array(
        // Desactiva a validación do formulario
        'validation_groups' => false,
    ));
	}

}
