<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Form/Type/VideoType.php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use AppBundle\Entity\Video;
use AppBundle\Entity\EducationLevel;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->addEventListener(FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
              $video = $event->getData();
              $form = $event->getForm();
              // Presenta un formulario ou outro en función do paso no que estemos
              if ($video->getStatus()=="") {
                // Se o Estado do vídeo está baleiro estamos no Paso 1
                // Só se pedirá a URL.
                // educationLevel e School ocultaranse na plantilla
                $form
                  ->add('url', UrlType::class, array('label'  => 'Url *',))
                  ->add('title', HiddenType::class)
                  ->add('educationLevel', EntityType::class, array('class' => 'AppBundle:EducationLevel',))
                  ->add('school', EntityType::class, array('class' => 'AppBundle:School'))
                  ->add('username', EntityType::class, array('class' => 'AppBundle:User'))
                  ->add('description', HiddenType::class)
                  ->add('comments', HiddenType::class)
                  ->add('status', HiddenType::class)
                  ->add('urlId', HiddenType::class)
                  ->add('provider', HiddenType::class)
                  ->add('proUploadDate', HiddenType::class)
                  ->add('duration', HiddenType::class)
                  ->add('urlThumbnail', HiddenType::class)
                  ->add('urlThumbnailMedium', HiddenType::class)
                  ->add('urlThumbnailHigh', HiddenType::class)
                  ;
              }
              elseif ($video->getStatus() == "EXISTE") {
                // Se o estado é "EXISTE" estamos no Paso 2.
                // Ocultamos a URL e outros datos calculados.
                // Amosamos título, educationLevel, school, description, etc.
                $form
                  ->add('url', HiddenType::class)
                  ->add('title', TextType::class, array('label'  => 'Título *'))
                  ->add('educationLevel', EntityType::class, array('label'  => 'Nivel Educativo *',
                                          'class' => 'AppBundle:EducationLevel',))
                  ->add('school', EntityType::class, array('label'  => 'Centro Educativo *',
                                          'class' => 'AppBundle:School',
                                          'query_builder' => function (EntityRepository $er) {
                                                             return $er->createQueryBuilder('u')
                                                                       ->orderBy('u.name', 'ASC');
                                                             }, ))
                  ->add('username', EntityType::class, array('class' => 'AppBundle:User'))
                  ->add('description', TextareaType::class, array('label'  => 'Descrición, Sinopse e Créditos','required' => false))
                  ->add('comments', TextareaType::class, array('label'  => 'Outros comentarios (privados para a organización)',
                                          'required' => false,))
                  ->add('status', HiddenType::class)
                  ->add('urlId', HiddenType::class)
                  ->add('provider', HiddenType::class)
                  ->add('proUploadDate', HiddenType::class)
                  ->add('duration', HiddenType::class)
                  ->add('urlThumbnail', HiddenType::class)
                  ->add('urlThumbnailMedium', HiddenType::class)
                  ->add('urlThumbnailHigh', HiddenType::class)
                  ;
                } else {
                  // Se houbero outro estado posible...
              }
          });
    }

  public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(array(
	        'data_class' => Video::class,
	    ));
	}

}
