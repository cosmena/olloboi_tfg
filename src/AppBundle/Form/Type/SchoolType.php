<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Form/Type/SchoolType.php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


use AppBundle\Entity\School;

class SchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('name', TextType::class, array('label' => 'Nome'))
      ->add('email', EmailType::class, array('label' => 'Correo-e', 'required' => false))
      ->add('address', TextType::class, array('label' => 'Enderezo', 'required' => false))
      ->add('town', TextType::class, array('label' => 'Localidade', 'required' => false))
      ->add('province', TextType::class, array('label' => 'Provincia', 'required' => false))
      ->add('cp', TextType::class, array('label' => 'Código Postal', 'required' => false))
      ->add('phone', TextType::class, array('label' => 'Teléfono', 'required' => false))
      ->add('coordx', TextType::class, array('label' => 'Coordenada X', 'required' => false))
      ->add('coordy', TextType::class, array('label' => 'Coordenada Y', 'required' => false))
      ->add('ownership', TextType::class, array('label' => 'Titularidade', 'required' => false))
      ->add('concerted', TextType::class, array('label' => 'Concertado?', 'required' => false))
      ->add('dependent', TextType::class, array('label' => 'Dependente?', 'required' => false));
      }


  public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(array(
	        'data_class' => School::class,
	    ));
	}

}
