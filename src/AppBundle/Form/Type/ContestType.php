<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// src/AppBundle/Form/Type/ContestType.php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


use AppBundle\Entity\Contest;

class ContestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('name', TextType::class, array('label' => 'Nome'))
      ->add('description', TextareaType::class, array('label' => 'Descrición', 'required' => false))
      ->add('bases', TextareaType::class, array('label' => 'Bases', 'required' => false))
      ->add('startRegistration', DateType::class, array('label' => 'Data Inicio Inscrición', 'required' => false, 'widget' => 'single_text','html5' => false,'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy',))
      ->add('endRegistration', DateType::class, array('label' => 'Data Fin Inscrición', 'required' => false, 'widget' => 'single_text','html5' => false,'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy',))
      ->add('startVoting', DateType::class, array('label' => 'Data Inicio Votacións', 'required' => false, 'widget' => 'single_text','html5' => false,'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy',))
      ->add('endVoting', DateType::class, array('label' => 'Data Fin Votacións', 'required' => false, 'widget' => 'single_text','html5' => false,'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy',))
      ->add('awardsDate', DateType::class, array('label' => 'Data Entrega de Premios', 'required' => false, 'widget' => 'single_text','html5' => false,'attr' => ['class' => 'js-datepicker'],'format' => 'dd/MM/yyyy',))
      ->add('awardsSchedule', TextareaType::class, array('label' => 'Programa da Entrega de Premios', 'required' => false))
      ->add('jury', TextareaType::class, array('label' => 'Xurado', 'required' => false))
      ->add('sponsors', TextareaType::class, array('label' => 'Patrocinadores', 'required' => false))
      ->add('finalReport', TextareaType::class, array('label' => 'Memoria Final', 'required' => false))
      ->add('url', UrlType::class, array('label'  => 'Url sitio web externo',  'required' => false));

      // Se se trata do administrador amósanse campos extra de edición
      if ($options['role'] == 'ROLE_ADMIN') {
        $builder->add('user', EntityType::class, array('class' => 'AppBundle:User', 'label' => 'Usuari@ organizador/a', 'required' => false,
                                                                                    'query_builder' => function (EntityRepository $er) {
                                                                                        return $er->createQueryBuilder('u')
                                                                                                  ->orderBy('u.username', 'ASC');
                                                                                                  },))
                ->add('published', CheckboxType::class, array('label' => 'Publicado?', 'required' => false));
      }
    }

  public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(array(
	        'data_class' => Contest::class,
          'role' => null,
	    ));
	}

}
