<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */


namespace AppBundle\Form\TypeFOS;

use Doctrine\ORM\EntityRepository;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProfileFormType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $this->buildUserForm($builder, $options);

      $builder->add('name', TextType::class, array('label'  => 'Nome','required' => false,))
              ->add('surname', TextType::class, array('label'  => 'Apelidos','required' => false,))
              ->add('email2', EmailType::class, array('label'  => 'Correo-e 2','required' => false,))
              ->add('email3', EmailType::class, array('label'  => 'Correo-e 3','required' => false,))
              ->add('notification', CheckboxType::class, array('label'  => 'Recibir notificacións?','required' => false,))
              ->add('phone', TextType::class, array('label'  => 'Teléfono','required' => false,))
              ->add('web', UrlType::class, array('label'  => 'Web','required' => false,))
              ->add('school', EntityType::class, array('label'  => 'Centro Educativo *',
                                                          'class' => 'AppBundle:School',
                                                          'query_builder' => function (EntityRepository $er) {
                                                                    return $er->createQueryBuilder('u')
                                                                    ->orderBy('u.name', 'ASC');
                                                                    },))
              ->add('current_password', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
                            'label' => 'Contrasinal Actual',
                            'translation_domain' => 'FOSUserBundle',
                            'mapped' => false,
                            'constraints' => new UserPassword(),))

          ;
  }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
        //return 'fos_user_profile';
    }

    public function getName()
    {
        return 'app_user_profile';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'Usuari@', 'translation_domain' => 'FOSUserBundle'))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'Correo-e', 'translation_domain' => 'FOSUserBundle'))
        ;
    }
}
