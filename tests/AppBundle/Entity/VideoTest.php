<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// tests/AppBundle/Entity/VideoTest.php
namespace Tests\AppBundle\Video;

use AppBundle\Entity\Video;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManagerInterface;

class VideoTest extends TestCase
{
    public function testUrlVimeoOK()
    {
      $ValorOrixinal = "https://vimeo.com/9316574";
      $ValorEsperado = "VIMEO";
      // Crea un obxecto Video
      $entityManager = $this->getMockBuilder(EntityManagerInterface::class);
      $video = new Video($entityManager);
      // Asigna unha URL de Vimeo e executa o método para comprobala
      $video->setUrl($ValorOrixinal);
      $video->isYoutubeOrVimeo();

      // assert that the video provider is ok!
      $this->assertEquals($ValorEsperado, $video->getProvider());
      $this->assertEquals("9316574", $video->getUrlId());
    }

    public function testUrlVimeoFail()
    {
      $entityManager = $this->getMockBuilder(EntityManagerInterface::class);
        // Crea un obxecto Video
        $video = new Video($entityManager);
        // Asigna unha URL de Vimeo e executa o método para comprobala
        $video->setUrl("https://veo.com/9316574");
        $video->isYoutubeOrVimeo();

        // assert that the video provider is ok!
        $this->assertNotEquals("VIMEO", $video->getProvider());
        $this->assertNotEquals("9316574", $video->getUrlId());
    }

}
