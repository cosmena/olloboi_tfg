<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// tests\AppBundle\Controller\VideoControllerTest.php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VideoControllerTest extends WebTestCase
{
    public function testPublicURLs()
    {
        $client = static::createClient();

// Listado de vídeos
        $crawler = $client->request('GET', '/videos');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Vídeos', $crawler->filter('h2')->text());

// Detalle dos vídeos

    }

}
