<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// tests\AppBundle\Controller\IndexControllerTest.php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testPublicURLs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Olloboi', $crawler->filter('h2')->text());

        $crawler = $client->request('GET', '/olloboi');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Sobre Olloboi', $crawler->filter('h2')->text());

        $crawler = $client->request('GET', '/prensa');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Prensa e Medios', $crawler->filter('h2')->text());

        $crawler = $client->request('GET', '/complices');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Cómplices', $crawler->filter('h2')->text());

        $crawler = $client->request('GET', '/crear');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Creador@s', $crawler->filter('h2')->text());

        $crawler = $client->request('GET', '/faq');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Preguntas e Respostas', $crawler->filter('h3')->text());

        $crawler = $client->request('GET', '/condicions');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Condicións de uso', $crawler->filter('h3')->text());
    }

}
