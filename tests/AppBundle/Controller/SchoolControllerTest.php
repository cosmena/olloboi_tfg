<?php
/* Copyright 2017 Óscar Núñez Aguado
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation <http://www.gnu.org/licenses/> */

// tests\AppBundle\Controller\SchoolControllerTest.php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SchoolControllerTest extends WebTestCase
{
    public function testListSchools()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/escolas');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Escolas', $crawler->filter('h2')->text());
    }

}
